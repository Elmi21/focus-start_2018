package ru.cft.fs.minigalieva.task3.view;

import ru.cft.fs.minigalieva.task3.Difficulty;
import ru.cft.fs.minigalieva.task3.database.Highscore;
import ru.cft.fs.minigalieva.task3.utils.Strings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.util.List;
import java.util.Map;

class StatisticsView {
    private static  final int IMAGE_SIZE=25;
    private JFrame statisticsFrame;
    private Map<Difficulty, List<Highscore>> statistics;

    StatisticsView(Map<Difficulty, List<Highscore>> statistics){
        this.statistics=statistics;
        statisticsFrame = new JFrame();
        statisticsFrame.setTitle(Strings.get("HIGHSCORES"));
        statisticsFrame.setIconImage(new ImageIcon(getClass().getResource("img/icon.png")).getImage());
        initStatisticsFrame();
        statisticsFrame.pack();
        statisticsFrame.setLocationRelativeTo(null);
        statisticsFrame.setVisible(true);
    }

    private void initLevelStatisticPanel(JPanel panel, Difficulty difficulty){

        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
        GridBagLayout panelLayout = new GridBagLayout();
        panel.setLayout(panelLayout);

        GridBagConstraints panelConstraints = new GridBagConstraints();
        panelConstraints.gridx=0;
        panelConstraints.gridy=0;

        JLabel  levelLabel = new JLabel(Strings.get("BEST_LEVEL_RESULTS")+ " "+(Strings.get(difficulty.name())));
        panelConstraints.insets = new Insets(5, 50, 5, 50);
        panel.add( levelLabel, panelConstraints);

        GridBagConstraints localPanelConstraints = new GridBagConstraints();
        List<Highscore> highscoresList = statistics.get(difficulty);

        JPanel localPanel = new JPanel();
        GridBagLayout localPanelLayout = new GridBagLayout();
        localPanel.setLayout(localPanelLayout);

        JLabel nameLabel = new JLabel(Strings.get("NAME"));
        localPanelConstraints.gridx=0;
        localPanelConstraints.gridy=0;
        localPanelConstraints.insets = new Insets(3, 10, 3, 10);
        localPanel.add(nameLabel, localPanelConstraints);
        JLabel dateLabel = new JLabel(Strings.get("DATE"));
        localPanelConstraints.gridx=1;
        localPanel.add(dateLabel, localPanelConstraints);
        JLabel timeLabel = new JLabel(Strings.get("TIME"));
        localPanelConstraints.gridx=2;
        localPanel.add(timeLabel, localPanelConstraints);

        localPanelConstraints.anchor=GridBagConstraints.WEST;
        localPanelConstraints.fill=GridBagConstraints.HORIZONTAL;
        for(int i=0; i< highscoresList.size(); i++)
        {
            Highscore highscore = highscoresList.get(i);
            nameLabel = new JLabel(highscore.getName());
            dateLabel = new JLabel(dateFormat.format(highscore.getCreatedDate()));
            timeLabel = new JLabel(String.valueOf(highscore.getTime()));

            localPanelConstraints.gridy=i+1;
            localPanelConstraints.gridx=0;
            localPanel.add(nameLabel, localPanelConstraints);

            localPanelConstraints.gridx=1;
            localPanel.add(dateLabel, localPanelConstraints);

            localPanelConstraints.gridx=2;
            localPanel.add(timeLabel, localPanelConstraints);
        }

        panelConstraints.gridx=0;
        panelConstraints.gridy=1;
        panelConstraints.fill=GridBagConstraints.HORIZONTAL;
        panelConstraints.anchor=GridBagConstraints.NORTH;
        panel.add(localPanel, panelConstraints);
    }
    private void initStatisticsFrame() {
        JTabbedPane tabbedPane = new JTabbedPane();

        Image winImage = ImageUtil.getImage("img/star.png");
        Image newWinImage = winImage != null ? winImage.getScaledInstance(IMAGE_SIZE, IMAGE_SIZE, Image.SCALE_SMOOTH) : null;
        ImageIcon icon = newWinImage != null ? new ImageIcon(newWinImage) : null;

        Difficulty[] difficulties = Difficulty.values();
        int i = 0;
        for (Difficulty difficulty : difficulties) {
            if (difficulty == Difficulty.SPECIAL || statistics.get(difficulty).isEmpty()) {
                continue;
            }
            JPanel panel = new JPanel();
            initLevelStatisticPanel(panel, difficulty);
            tabbedPane.addTab(Strings.get(difficulty.name()), icon, panel);
            tabbedPane.setMnemonicAt(i, KeyEvent.VK_1 + i);
            i++;
        }
        statisticsFrame.add(tabbedPane);
    }
}
