package ru.cft.fs.minigalieva.task3;

public enum AdoptionSettings {
    SETTINGS_CHANGED,
    SETTINGS_NOT_CHANGED,
    INVALID_SETTINGS
}
