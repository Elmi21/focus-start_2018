package ru.cft.fs.minigalieva.task3.model.game;

import ru.cft.fs.minigalieva.task3.Box;
import ru.cft.fs.minigalieva.task3.BoxGameState;
import ru.cft.fs.minigalieva.task3.BoxUserState;
import ru.cft.fs.minigalieva.task3.Coordinate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class GameField {
    private int rows;
    private int cols;
    private int bombs;
    private int allCoordinatesNumber;
    private int closedBoxesNumber;
    private Box[][] boxes;

    GameField(int rows, int cols, int bombs) {
        this.bombs = bombs;
        this.rows = rows;
        this.cols = cols;
        closedBoxesNumber = rows * cols;
        this.allCoordinatesNumber = rows * cols;
    }

    void initField() {

        boxes = new Box[rows][cols];
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                {
                    boxes[x][y] = new Box(BoxGameState.ZERO, BoxUserState.CLOSED, new Coordinate(x, y));
                }
            }
        }
        for (int i = 0; i < bombs; i++) {
            placeBomb();
        }
    }

    private Coordinate getRandomCoordinate() {
        Random random = new Random();
        return (new Coordinate(random.nextInt(rows), random.nextInt(cols)));
    }

    List<Coordinate> getNeighbourBoxes(Coordinate coordinate) {
        Coordinate candidateForCoordinateAround;
        ArrayList<Coordinate> coordinatesList = new ArrayList<>();
        for (int x = coordinate.getX() - 1; x <= coordinate.getX() + 1; x++) {
            for (int y = coordinate.getY() - 1; y <= coordinate.getY() + 1; y++) {
                candidateForCoordinateAround = new Coordinate(x, y);
                if (isCoordinateInField(candidateForCoordinateAround) && (!candidateForCoordinateAround.equals(coordinate))) {
                    coordinatesList.add(candidateForCoordinateAround);
                }
            }
        }
        return coordinatesList;
    }

    boolean isCoordinateInField(Coordinate coordinate) {
        return coordinate.getX() >= 0 && coordinate.getX() < rows && coordinate.getY() >= 0 &&
                coordinate.getY() < cols;
    }

    int getAllCoordinatesNumber() {
        return allCoordinatesNumber;
    }

    BoxUserState getBoxUserState(Coordinate coordinate) {
        if (isCoordinateInField(coordinate))
            return boxes[coordinate.getX()][coordinate.getY()].getBoxUserState();
        else
            return null;
    }

    void setBoxUserState(Coordinate coordinate, BoxUserState boxUserState) {
        if (isCoordinateInField(coordinate))
            boxes[coordinate.getX()][coordinate.getY()].setBoxUserState(boxUserState);
        if (boxUserState == BoxUserState.OPENED)
            closedBoxesNumber--;
    }

    BoxGameState getBoxGameState(Coordinate coordinate) {
        if (isCoordinateInField(coordinate))
            return boxes[coordinate.getX()][coordinate.getY()].getBoxGameState();
        else
            return null;
    }

    private void setBoxGameState(Coordinate coordinate, BoxGameState boxGameState) {
        if (isCoordinateInField(coordinate))
            boxes[coordinate.getX()][coordinate.getY()].setBoxGameState(boxGameState);
    }

    private void placeBomb() {
        while (true) {
            Coordinate coordinate = getRandomCoordinate();
            if (BoxGameState.BOMB != getBoxGameState(coordinate)) {
                setBoxGameState(coordinate, BoxGameState.BOMB);
                putNumbersAroundBomb(coordinate);
                break;
            }
        }

    }

    private void putNumbersAroundBomb(Coordinate coordinate) {
        for (Coordinate localCoordinate : getNeighbourBoxes(coordinate)) {
            if (getBoxGameState(localCoordinate) != BoxGameState.BOMB) {
                setBoxGameState(localCoordinate, getBoxGameState(localCoordinate).getNextBoxState());
            }
        }
    }

    int getClosedBoxesNumber() {
        return closedBoxesNumber;
    }

    int getFlaggedNeighbourBoxesNumber(Coordinate coordinate) {
        int count = 0;
        for (Coordinate localCoordinate : getNeighbourBoxes(coordinate)) {
            if (getBoxUserState(localCoordinate) == BoxUserState.FLAGGED) {
                count++;
            }
        }
        return count;
    }

    void setClosedBoxesNumber(int closedBoxesNumber) {
        this.closedBoxesNumber = closedBoxesNumber;
    }
}
