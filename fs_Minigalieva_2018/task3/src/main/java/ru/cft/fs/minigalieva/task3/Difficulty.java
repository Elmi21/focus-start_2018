package ru.cft.fs.minigalieva.task3;

public enum Difficulty {
    BEGINNER,
    AMATEUR,
    PROFESSIONAL,
    SPECIAL
}
