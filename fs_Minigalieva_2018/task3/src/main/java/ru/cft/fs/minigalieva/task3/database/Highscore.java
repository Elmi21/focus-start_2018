package ru.cft.fs.minigalieva.task3.database;

import ru.cft.fs.minigalieva.task3.Difficulty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Highscores")
public class Highscore {
    @Id
    @GeneratedValue
    private int id;
    @Column
    private Difficulty difficulty;
    @Column
    private Date createdDate;
    @Column
    private int time;
    @Column
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

