package ru.cft.fs.minigalieva.task3;

public class Box {
    private Coordinate coordinate;
    private BoxGameState boxGameState;
    private BoxUserState boxUserState;

    public Box(BoxGameState boxGameState, BoxUserState boxUserState, Coordinate coordinate) {
        this.boxGameState = boxGameState;
        this.boxUserState = boxUserState;
        this.coordinate = coordinate;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public BoxUserState getBoxUserState() {
        return boxUserState;
    }

    public void setBoxUserState(BoxUserState boxUserState) {
        this.boxUserState = boxUserState;
    }

    public void setBoxGameState(BoxGameState boxGameState) {
        this.boxGameState = boxGameState;
    }

    public BoxGameState getBoxGameState() {
        return boxGameState;
    }

    public String getCurrentBoxState() {
        if (getBoxUserState() == BoxUserState.OPENED)
            return getBoxGameState().toString();
        else
            return getBoxUserState().toString();
    }
}
