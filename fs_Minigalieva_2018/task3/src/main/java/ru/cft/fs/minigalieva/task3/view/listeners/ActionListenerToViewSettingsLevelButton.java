package ru.cft.fs.minigalieva.task3.view.listeners;

import ru.cft.fs.minigalieva.task3.Difficulty;
import ru.cft.fs.minigalieva.task3.view.SettingsView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionListenerToViewSettingsLevelButton implements ActionListener {

    private SettingsView settingsView;
    public ActionListenerToViewSettingsLevelButton(SettingsView settingsView){
        this.settingsView=settingsView;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Difficulty difficulty = (Difficulty) ((JRadioButton) e.getSource()).getClientProperty("difficulty");
        settingsView.enableLevelButton(difficulty);
        settingsView.setDifficulty(difficulty);
    }
}
