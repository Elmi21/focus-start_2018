package ru.cft.fs.minigalieva.task3.model.settings;

import ru.cft.fs.minigalieva.task3.AdoptionSettings;
import ru.cft.fs.minigalieva.task3.Difficulty;
import ru.cft.fs.minigalieva.task3.listeners.SettingsModelListener;

import java.util.ArrayList;
import java.util.List;

public class SettingsModel {
    public static final int BEGINNER_ROWS = 9;
    public static final int BEGINNER_COLS = 9;
    public static final int BEGINNER_BOMBS = 10;
    public static final int AMATEUR_ROWS = 16;
    public static final int AMATEUR_COLS = 16;
    public static final int AMATEUR_BOMBS = 40;
    public static final int PROFESSIONAL_ROWS = 16;
    public static final int PROFESSIONAL_COLS = 30;
    public static final int PROFESSIONAL_BOMBS = 99;
    public static final int MIN_COLS = 9;
    public static final int MAX_COLS = 30;
    public static final int MIN_ROWS = 9;
    public static final int MAX_ROWS = 24;
    public static final int MIN_BOMBS = 10;
    public static final int MAX_BOMBS = 612;
    private Difficulty difficulty;
    private int rows;
    private int cols;
    private int bombs;
    private List<SettingsModelListener> observers;
    private AdoptionSettings adoptionSettings;

    public SettingsModel() {
        this.difficulty = Difficulty.BEGINNER;
        this.rows = BEGINNER_ROWS;
        this.cols = BEGINNER_COLS;
        this.bombs = BEGINNER_BOMBS;
        setLevel(difficulty, rows, cols, bombs);
        adoptionSettings = AdoptionSettings.SETTINGS_CHANGED;
        observers = new ArrayList<>();
    }

    private AdoptionSettings setLevel(Difficulty difficulty, Integer rows, Integer cols, Integer
            bombs) {
        if ((this.difficulty == difficulty && (this.difficulty != Difficulty.SPECIAL) ||
                (this.rows == rows && this.cols == cols && this.bombs == bombs && this.difficulty == Difficulty.SPECIAL))) {
            return AdoptionSettings.SETTINGS_NOT_CHANGED;
        }
        switch (difficulty) {
            case BEGINNER:
                setRows(BEGINNER_ROWS);
                setCols(BEGINNER_COLS);
                setBombs(BEGINNER_BOMBS);
                break;
            case AMATEUR:
                setRows(AMATEUR_ROWS);
                setCols(AMATEUR_COLS);
                setBombs(AMATEUR_BOMBS);
                break;
            case PROFESSIONAL:
                setRows(PROFESSIONAL_ROWS);
                setCols(PROFESSIONAL_COLS);
                setBombs(PROFESSIONAL_BOMBS);
                break;
            case SPECIAL:
                setRows(rows);
                setCols(cols);
                setBombs(bombs);
                break;
            default:
                break;
        }
        setDifficulty(difficulty);
        if (validate()) {
            adoptionSettings = AdoptionSettings.SETTINGS_CHANGED;
            sendToObservers();
        } else
            adoptionSettings = AdoptionSettings.INVALID_SETTINGS;

        return adoptionSettings;
    }

    public void register(SettingsModelListener outlet) {
        observers.add(outlet);
        outlet.updateSettingsModel(this);
    }

    private void sendToObservers() {
        for (SettingsModelListener outlet : this.observers) {
            outlet.updateSettingsModel(this);
        }
    }

    public int getRows() {
        return rows;
    }

    private void setRows(int rows) {
        if (this.rows != rows) {
            this.rows = rows;
        }
    }

    public int getCols() {
        return cols;
    }

    private void setCols(int cols) {
        this.cols = cols;
    }

    public int getBombs() {
        return bombs;
    }

    private void setBombs(int bombsNum) {
        this.bombs = bombsNum;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    private void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    private boolean validate() {
        return (rows <= MAX_ROWS) && (rows >= MIN_ROWS) && (cols <= MAX_COLS) && (cols >= MIN_COLS) &&
                (bombs <= MAX_BOMBS && bombs <= Math.floor(0.85 * rows * cols)) && (bombs >= MIN_BOMBS);
    }

    public void setSettings(Difficulty difficulty, int height, int width, int bombs) {

        adoptionSettings = AdoptionSettings.SETTINGS_NOT_CHANGED;
        switch (difficulty) {
            case BEGINNER:
            case AMATEUR:
            case PROFESSIONAL:
                adoptionSettings = setLevel(difficulty, 0, 0, 0);
                break;
            case SPECIAL:
                adoptionSettings = setLevel(difficulty, height, width, bombs);
                break;
            default:
                break;
        }
    }

    public AdoptionSettings getAdoptionSettings() {
        return adoptionSettings;
    }

}
