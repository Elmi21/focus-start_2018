package ru.cft.fs.minigalieva.task3.view.listeners;

import ru.cft.fs.minigalieva.task3.view.SettingsView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public  class ActionListenerToViewSettingsButtonCancel implements ActionListener {
    private SettingsView settingsView;
    public ActionListenerToViewSettingsButtonCancel(SettingsView settingsView){
        this.settingsView=settingsView;
    }
    @Override
    public void actionPerformed(ActionEvent e) { settingsView.closeSettingsView();
    }
}
