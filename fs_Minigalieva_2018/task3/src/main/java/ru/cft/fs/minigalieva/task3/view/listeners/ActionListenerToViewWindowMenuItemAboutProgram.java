package ru.cft.fs.minigalieva.task3.view.listeners;

import ru.cft.fs.minigalieva.task3.view.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionListenerToViewWindowMenuItemAboutProgram implements ActionListener {
    private View view;
    public ActionListenerToViewWindowMenuItemAboutProgram(View view){
        this.view=view;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        view.showAboutProgramView();
    }
}
