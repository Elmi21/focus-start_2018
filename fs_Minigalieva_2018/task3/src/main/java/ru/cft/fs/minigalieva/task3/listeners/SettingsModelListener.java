package ru.cft.fs.minigalieva.task3.listeners;

import ru.cft.fs.minigalieva.task3.model.settings.SettingsModel;

public interface SettingsModelListener {
    void updateSettingsModel(SettingsModel settingsModel);
}
