package ru.cft.fs.minigalieva.task3.view;

import ru.cft.fs.minigalieva.task3.Difficulty;
import ru.cft.fs.minigalieva.task3.listeners.SettingsModelListener;
import ru.cft.fs.minigalieva.task3.listeners.SettingsViewListener;
import ru.cft.fs.minigalieva.task3.AdoptionSettings;
import ru.cft.fs.minigalieva.task3.model.settings.SettingsModel;
import ru.cft.fs.minigalieva.task3.utils.Strings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class SettingsView implements SettingsModelListener {

    private static final String DIFFICULTY_PROPERTY = "difficulty";
    private static final int BUTTON_WIDTH = 85;
    private static final int BUTTON_HEIGHT = 23;
    private JFrame settingsFrame;
    private JRadioButton beginnerLevelButton;
    private JRadioButton amateurLevelButton;
    private JRadioButton professionalLevelButton;
    private JRadioButton specialLevelButton;
    private JSpinner textFieldWidth;
    private JSpinner textFieldHeight;
    private JSpinner textFieldBombsNum;
    private GridBagConstraints gridBagConstraintsSettingsFrame;
    private JButton buttonOk;
    private JButton buttonCancel;
    private JPanel specialLevelParameters;
    private Difficulty difficulty;
    private List<SettingsViewListener> observers;

    public SettingsView() {
        initSettingsFrame();
    }

    private void initSettingsFrame() {
        observers=new ArrayList<>();
        settingsFrame = new JFrame();
        settingsFrame.setTitle(Strings.get("SETTINGS_TITLE"));
        settingsFrame.setIconImage(new ImageIcon(getClass().getResource("img/icon.png")).getImage());
        GridBagLayout gridLayoutSettingsFrame = new GridBagLayout();
        gridBagConstraintsSettingsFrame = new GridBagConstraints();
        gridBagConstraintsSettingsFrame.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsSettingsFrame.ipadx = 0;
        gridBagConstraintsSettingsFrame.ipady = 0;
        settingsFrame.setLayout(gridLayoutSettingsFrame);
        initLevelsPanel();
        initButtonsPanel();
        settingsFrame.pack();
        settingsFrame.setLocationRelativeTo(null);
        settingsFrame.setResizable(false);
        difficulty=Difficulty.BEGINNER;
    }

    private void initLevelsPanel() {
        JPanel levelsPanel = new JPanel();
        GridBagLayout gridBagLayoutLevelPanel = new GridBagLayout();
        levelsPanel.setLayout(gridBagLayoutLevelPanel);

        GridBagConstraints gridBagConstraintsPanel = new GridBagConstraints();
        gridBagConstraintsPanel.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsPanel.gridx = 0;
        gridBagConstraintsPanel.gridy = 0;
        gridBagConstraintsPanel.weightx=2;
        gridBagConstraintsPanel.insets=new Insets(0,160,0,0);

        JLabel jLabelLevel = new JLabel(Strings.get("LEVEL"));
        levelsPanel.add(jLabelLevel, gridBagConstraintsPanel);

        gridBagConstraintsPanel.insets=new Insets(0,0,0,0);
        String levelFormat = "<html>%s<br/>%s</html>";
        String levelDescription = "LEVEL_DESCRIPTION";
        String beginnerDescription = Strings.get(levelDescription, SettingsModel.BEGINNER_BOMBS, SettingsModel
                .BEGINNER_ROWS, SettingsModel.BEGINNER_COLS);
        beginnerLevelButton = new JRadioButton(String.format(levelFormat, Strings.get("BEGINNER"),
                beginnerDescription), true);
        beginnerLevelButton.putClientProperty(DIFFICULTY_PROPERTY, Difficulty.BEGINNER);
        gridBagConstraintsPanel.gridx = 0;
        gridBagConstraintsPanel.gridy = 1;
        levelsPanel.add(beginnerLevelButton, gridBagConstraintsPanel);

        String amateurDescription = Strings.get(levelDescription, SettingsModel.AMATEUR_BOMBS, SettingsModel
                .AMATEUR_ROWS, SettingsModel.AMATEUR_COLS);
        amateurLevelButton = new JRadioButton(String.format(levelFormat, Strings.get("AMATEUR"),
                amateurDescription), false);
        amateurLevelButton.putClientProperty(DIFFICULTY_PROPERTY, Difficulty.AMATEUR);
        gridBagConstraintsPanel.gridx = 0;
        gridBagConstraintsPanel.gridy = 2;
        levelsPanel.add(amateurLevelButton, gridBagConstraintsPanel);

        String professionalDescription = Strings.get(levelDescription, SettingsModel.PROFESSIONAL_BOMBS, SettingsModel
                .PROFESSIONAL_ROWS, SettingsModel.PROFESSIONAL_COLS);
        professionalLevelButton = new JRadioButton(String.format(levelFormat, Strings.get("PROFESSIONAL"),
                professionalDescription), false);
        professionalLevelButton.putClientProperty(DIFFICULTY_PROPERTY, Difficulty.PROFESSIONAL);
        gridBagConstraintsPanel.gridx = 0;
        gridBagConstraintsPanel.gridy = 3;
        levelsPanel.add(professionalLevelButton, gridBagConstraintsPanel);

        specialLevelButton = new JRadioButton(Strings.get("SPECIAL"), false);
        specialLevelButton.putClientProperty(DIFFICULTY_PROPERTY, Difficulty.SPECIAL);
        gridBagConstraintsPanel.gridx = 1;
        gridBagConstraintsPanel.gridy = 1;
        levelsPanel.add(specialLevelButton, gridBagConstraintsPanel);

        addActionListenerToViewSettingLevelButton(e -> {
            Difficulty localDifficulty = (Difficulty) ((JRadioButton) e.getSource()).getClientProperty(DIFFICULTY_PROPERTY);
            enableLevelButton(localDifficulty);
            setDifficulty(localDifficulty);
        });

        ButtonGroup group = new ButtonGroup();
        group.add(beginnerLevelButton);
        group.add(amateurLevelButton);
        group.add(professionalLevelButton);
        group.add(specialLevelButton);

        settingsFrame.add(levelsPanel, gridBagConstraintsSettingsFrame);

        specialLevelParameters = new JPanel();
        specialLevelParameters.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraintsSpecialLevelsParameters = new GridBagConstraints();

        JLabel labelWidth = new JLabel(Strings.get("WIDTH", SettingsModel.MIN_COLS, SettingsModel.MAX_COLS));
        gridBagConstraintsSpecialLevelsParameters.gridx = 0;
        gridBagConstraintsSpecialLevelsParameters.gridy = 0;
        gridBagConstraintsSpecialLevelsParameters.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraintsSpecialLevelsParameters.insets=new Insets(5,5,5,5);
        labelWidth.setEnabled(false);
        specialLevelParameters.add(labelWidth, gridBagConstraintsSpecialLevelsParameters);

        textFieldWidth = new JSpinner(new SpinnerNumberModel(SettingsModel.MIN_COLS, SettingsModel.MIN_COLS,
                SettingsModel.MAX_COLS, 1));
        gridBagConstraintsSpecialLevelsParameters.gridx = 1;
        gridBagConstraintsSpecialLevelsParameters.gridy = 0;
        textFieldWidth.setEnabled(false);
        specialLevelParameters.add(textFieldWidth, gridBagConstraintsSpecialLevelsParameters);

        JLabel labelHeight = new JLabel(Strings.get("HEIGHT", SettingsModel.MIN_ROWS, SettingsModel.MAX_ROWS));
        gridBagConstraintsSpecialLevelsParameters.gridx = 0;
        gridBagConstraintsSpecialLevelsParameters.gridy = 1;
        labelHeight.setEnabled(false);
        specialLevelParameters.add(labelHeight, gridBagConstraintsSpecialLevelsParameters);

        textFieldHeight = new JSpinner(new SpinnerNumberModel(SettingsModel.MIN_ROWS, SettingsModel.MIN_ROWS, SettingsModel.MAX_ROWS, 1));
        configureSpinner(textFieldHeight);
        gridBagConstraintsSpecialLevelsParameters.gridx = 1;
        gridBagConstraintsSpecialLevelsParameters.gridy = 1;
        textFieldHeight.setEnabled(false);
        specialLevelParameters.add(textFieldHeight, gridBagConstraintsSpecialLevelsParameters);

        JLabel labelBombsNum = new JLabel(Strings.get("BOMBS_NUM", SettingsModel.MIN_BOMBS, SettingsModel
                .MAX_BOMBS));
        gridBagConstraintsSpecialLevelsParameters.gridx = 0;
        gridBagConstraintsSpecialLevelsParameters.gridy = 2;
        labelBombsNum.setEnabled(false);
        specialLevelParameters.add(labelBombsNum, gridBagConstraintsSpecialLevelsParameters);

        textFieldBombsNum = new JSpinner(new SpinnerNumberModel(SettingsModel.MIN_BOMBS, SettingsModel.MIN_BOMBS,
                SettingsModel.MAX_BOMBS, 1));
        configureSpinner(textFieldBombsNum);
        gridBagConstraintsSpecialLevelsParameters.gridx = 1;
        gridBagConstraintsSpecialLevelsParameters.gridy = 2;
        textFieldBombsNum.setEnabled(false);
        specialLevelParameters.add(textFieldBombsNum, gridBagConstraintsSpecialLevelsParameters);

        gridBagConstraintsPanel.gridx = 1;
        gridBagConstraintsPanel.gridy = 2;
        levelsPanel.add(specialLevelParameters, gridBagConstraintsPanel);
    }


    private void initButtonsPanel() {
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraintsPanel = new GridBagConstraints();
        buttonOk = new JButton(Strings.get("OK"));
        buttonOk.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
        buttonOk.addActionListener(e -> {
            sendToObservers();
            closeSettingsView();
        });
        gridBagConstraintsPanel.anchor=GridBagConstraints.CENTER;
        gridBagConstraintsPanel.insets=new Insets(5,20,0,20);
        gridBagConstraintsPanel.gridx = 0;
        gridBagConstraintsPanel.gridy = 0;
        buttonsPanel.add(buttonOk, gridBagConstraintsPanel);

        buttonCancel = new JButton(Strings.get("CANCEL"));
        buttonCancel.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
        buttonCancel.addActionListener(e -> closeSettingsView());
        gridBagConstraintsPanel.gridx = 1;
        gridBagConstraintsPanel.gridy = 0;
        buttonsPanel.add(buttonCancel, gridBagConstraintsPanel);

        gridBagConstraintsSettingsFrame.gridx = 0;
        gridBagConstraintsSettingsFrame.gridy = 1;
        settingsFrame.add(buttonsPanel, gridBagConstraintsSettingsFrame);
    }

    private JButton getButtonCancel() {
        return buttonCancel;
    }

    public void enableLevelButton(Difficulty difficulty) {
        switch (difficulty){
            case BEGINNER:
            case AMATEUR:
            case PROFESSIONAL:
                setPanelEnabled(specialLevelParameters, false);
                break;
            case SPECIAL:
                setPanelEnabled(specialLevelParameters, true);
                break;
        }
    }

    private int getTextFieldWidthValue() {
        return (int)textFieldWidth.getValue();
    }

    private int getTextFieldHeightValue() {
        return (int)textFieldHeight.getValue();
    }

    private int getTextFieldBombsNumValue() {
        return (int)textFieldBombsNum.getValue();
    }

    public void showSettingsView() {
        settingsFrame.setVisible(true);
    }

    public void closeSettingsView() {
        settingsFrame.setVisible(false);
    }

    public void destroySettingsFrame() {
        settingsFrame.dispose();
    }

    private static void setPanelEnabled(JPanel panel, Boolean isEnabled) {
        panel.setEnabled(isEnabled);

        Component[] components = panel.getComponents();

        for (Component component : components) {
            if (component instanceof JPanel) {
                setPanelEnabled((JPanel) component, isEnabled);
            }

            component.setEnabled(isEnabled);
        }
    }

    public void addActionListenerToViewSettingButtonOk(ActionListener actionListener) {
        buttonOk.addActionListener(actionListener);
    }

    public void addActionListenerToViewSettingButtonCancel(ActionListener actionListener) {
        getButtonCancel().addActionListener(actionListener);
    }

    private void addActionListenerToViewSettingLevelButton(ActionListener actionListener) {
        beginnerLevelButton.addActionListener(actionListener);
        amateurLevelButton.addActionListener(actionListener);
        professionalLevelButton.addActionListener(actionListener);
        specialLevelButton.addActionListener(actionListener);
    }


    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    private void showIncorrectParametersMessageDialog(){
        JOptionPane.showMessageDialog(settingsFrame, Strings.get("ERROR_MESSAGE"), Strings.get("ERROR"),
                JOptionPane.ERROR_MESSAGE);
    }

    private void configureSpinner(JSpinner spinner) {
        Dimension d = spinner.getPreferredSize();
        d.width = 50;
        spinner.setPreferredSize(d);
    }

    @Override
    public void updateSettingsModel(SettingsModel settingsModel) {
        if (settingsModel.getAdoptionSettings()==AdoptionSettings.INVALID_SETTINGS){
            showIncorrectParametersMessageDialog();
        }
        difficulty=settingsModel.getDifficulty();
        if(difficulty==Difficulty.SPECIAL)
        {
            textFieldWidth.setValue(settingsModel.getCols());
            textFieldHeight.setValue(settingsModel.getRows());
            textFieldBombsNum.setValue(settingsModel.getBombs());
        }
    }

    public void register(SettingsViewListener outlet) {
        observers.add(outlet);
    }

    public void sendToObservers() {
        for (SettingsViewListener outlet : this.observers) {
            outlet.updateSettings(getDifficulty(), getTextFieldHeightValue(), getTextFieldWidthValue(), getTextFieldBombsNumValue());
        }
    }
}
