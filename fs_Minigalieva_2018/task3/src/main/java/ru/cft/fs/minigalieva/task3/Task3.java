package ru.cft.fs.minigalieva.task3;

import ru.cft.fs.minigalieva.task3.contoller.Controller;
import ru.cft.fs.minigalieva.task3.model.game.Game;
import ru.cft.fs.minigalieva.task3.model.settings.SettingsModel;
import ru.cft.fs.minigalieva.task3.model.timer.TimerModel;
import ru.cft.fs.minigalieva.task3.view.SettingsView;
import ru.cft.fs.minigalieva.task3.view.View;

import javax.swing.*;

public class Task3 {
    public static void main(String[] args) {
        TimerModel timerModel = new TimerModel();
        Game game = new Game(timerModel);
        SettingsModel settingsModel = new SettingsModel();
        Controller controller = new Controller(game, settingsModel);
        SettingsView settingsView = new SettingsView();
        View view = new View(settingsView);
        view.addActionListenerToViewWindowMenuItemNewGame(e -> controller.restartGame());
        view.addActionListenerToViewWindowMenuItemStatistics(e -> controller.showStatistics());
        settingsView.register(controller);
        view.register(controller);
        timerModel.register(view);
        settingsModel.register(view);
        settingsModel.register(settingsView);
        settingsModel.register(game);
        game.register(view);

        SwingUtilities.invokeLater(
                view::showViewWindow);

        Runtime.getRuntime().addShutdownHook(new Thread(timerModel::stop));
    }
}