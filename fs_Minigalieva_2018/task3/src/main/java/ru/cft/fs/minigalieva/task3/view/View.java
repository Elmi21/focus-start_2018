package ru.cft.fs.minigalieva.task3.view;

import ru.cft.fs.minigalieva.task3.Difficulty;
import ru.cft.fs.minigalieva.task3.GameState;
import ru.cft.fs.minigalieva.task3.ModelUpdateResult;
import ru.cft.fs.minigalieva.task3.listeners.GameListener;
import ru.cft.fs.minigalieva.task3.listeners.SettingsModelListener;
import ru.cft.fs.minigalieva.task3.listeners.ViewListener;
import ru.cft.fs.minigalieva.task3.database.Highscore;
import ru.cft.fs.minigalieva.task3.listeners.TimerModelListener;
import ru.cft.fs.minigalieva.task3.AdoptionSettings;
import ru.cft.fs.minigalieva.task3.model.settings.SettingsModel;
import ru.cft.fs.minigalieva.task3.utils.Strings;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

public class View implements GameListener, SettingsModelListener, TimerModelListener {

    private JFrame mainFrame;
    private JDialog aboutDialog;
    private GridBagConstraints frameConstraints;
    private JMenuItem jMenuItemNewGame;
    private JMenuItem jMenuItemExit;
    private JMenuItem jMenuItemSettings;
    private FieldView fieldView;
    private JMenuItem jMenuItemHighscores;
    private JLabel bombsText;
    private JLabel timerText;
    private static final int BUTTON_SIZE = 25;
    private static final int EXTRA_ITEM_SIZE = 35;
    private static final int EXTRA_ITEM_HEIGHT = 23;
    private static final int EXTRA_ITEM_WIDTH = 45;
    private static final int WIN_ITEM_SIZE = 50;
    private Image winNewImg;
    private JMenuItem aboutProgram;
    private List<ViewListener> observers;
    private SettingsView settingsView;

    public View(SettingsView settingsView) {
        this.settingsView = settingsView;
        mainFrame = new JFrame();
        initMainFrame();
    }

    private void initMainFrame() {
        observers = new ArrayList<>();
        mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setTitle(Strings.get("MAIN_TITLE"));
        mainFrame.setLayout(new GridBagLayout());
        frameConstraints = new GridBagConstraints();
        frameConstraints.fill = GridBagConstraints.HORIZONTAL;
        initMenu();
        initExtraOptionsPanel();
        initAboutProgramView();
        Image winImg = ImageUtil.getImage("img/win.png");
        winNewImg = winImg != null ? winImg.getScaledInstance(WIN_ITEM_SIZE, WIN_ITEM_SIZE, Image.SCALE_SMOOTH) : null;
        mainFrame.setIconImage(new ImageIcon(getClass().getResource("img/icon.png")).getImage());
        mainFrame.setResizable(false);

        UIManager.put("OptionPane.yesButtonText", Strings.get("YES"));
        UIManager.put("OptionPane.noButtonText", Strings.get("NO"));
        UIManager.put("OptionPane.cancelButtonText", Strings.get("CANCEL"));
    }

    private void initAboutProgramView() {
        aboutDialog = new JDialog(mainFrame, Strings.get("INFO_MINESWEEPER"), true);
        JPanel aboutProgramPanel = new JPanel();
        GridBagLayout aboutProgramLayout = new GridBagLayout();
        aboutProgramPanel.setLayout(aboutProgramLayout);
        GridBagConstraints aboutProgramConstraints = new GridBagConstraints();

        aboutProgramConstraints.insets = new Insets(10, 25, 0, 10);
        aboutProgramConstraints.anchor = GridBagConstraints.WEST;
        aboutProgramConstraints.gridx = 0;
        aboutProgramConstraints.gridy = 0;
        aboutProgramPanel.add(new JLabel(Strings.get("PROGRAM_TITLE_LABEL")), aboutProgramConstraints);
        aboutProgramConstraints.insets = new Insets(0, 25, 0, 10);
        aboutProgramConstraints.gridy = 1;
        aboutProgramPanel.add(new JLabel(Strings.get("PROGRAM_VERSION_LABEL")), aboutProgramConstraints);
        aboutProgramConstraints.gridy = 2;
        aboutProgramConstraints.insets = new Insets(0, 25, 10, 10);
        aboutProgramPanel.add(new JLabel(Strings.get("PROGRAM_AUTHOR_LABEL")), aboutProgramConstraints);

        aboutProgramConstraints.insets = new Insets(10, 10, 0, 25);
        aboutProgramConstraints.gridy = 0;
        aboutProgramConstraints.gridx = 1;
        aboutProgramPanel.add(new JLabel(Strings.get("PROGRAM_TITLE")), aboutProgramConstraints);
        aboutProgramConstraints.insets = new Insets(0, 10, 0, 25);
        aboutProgramConstraints.gridy = 1;
        aboutProgramPanel.add(new JLabel(Strings.get("PROGRAM_VERSION")), aboutProgramConstraints);
        aboutProgramConstraints.gridy = 2;
        aboutProgramConstraints.insets = new Insets(0, 10, 10, 25);
        aboutProgramPanel.add(new JLabel(Strings.get("PROGRAM_AUTHOR")), aboutProgramConstraints);
        JPanel buttonsPanel = new JPanel();
        JButton okButton = new JButton("Ok");
        buttonsPanel.add(okButton);
        aboutDialog.getContentPane().add(aboutProgramPanel, "North");
        aboutDialog.getContentPane().add(buttonsPanel, "South");
        okButton.addActionListener(evt -> aboutDialog.setVisible(false));
        aboutDialog.pack();
        aboutDialog.setLocationRelativeTo(null);
    }

    public void showAboutProgramView() {
        aboutDialog.setVisible(true);
    }

    private void showStatisticsView(Map<Difficulty, List<Highscore>> statistics) {
        new StatisticsView(statistics);
    }

    private void initExtraOptionsPanel() {
        Border border = BorderFactory.createLineBorder(Color.GRAY, 2, true);

        JPanel extraOptionsPanel = new JPanel();
        extraOptionsPanel.setLayout(new GridBagLayout());
        GridBagConstraints panelConstraints = new GridBagConstraints();
        panelConstraints.fill = GridBagConstraints.HORIZONTAL;
        Image timerImg = ImageUtil.getImage("img/timer.png");
        Image timerNewImg = timerImg != null ? timerImg.getScaledInstance(EXTRA_ITEM_SIZE, EXTRA_ITEM_SIZE, Image.SCALE_SMOOTH) : null;
        JLabel timerLabel = new JLabel(timerNewImg != null ? new ImageIcon(timerNewImg) : null);
        panelConstraints.gridx = 0;
        panelConstraints.gridy = 0;
        extraOptionsPanel.add(timerLabel, panelConstraints);
        timerText = new JLabel();
        timerText.setOpaque(true);
        timerText.setBackground(Color.WHITE);
        timerText.setBorder(border);
        timerText.setPreferredSize(new Dimension(EXTRA_ITEM_WIDTH, EXTRA_ITEM_HEIGHT));
        timerText.setHorizontalAlignment(JTextField.CENTER);
        timerText.setFont(new Font(Font.SERIF, Font.BOLD, 20));
        panelConstraints.gridx = 1;
        panelConstraints.gridy = 0;
        extraOptionsPanel.add(timerText, panelConstraints);

        panelConstraints.weightx = 1;
        panelConstraints.weighty = 0;
        panelConstraints.gridx = 2;
        panelConstraints.gridy = 0;
        extraOptionsPanel.add(Box.createHorizontalGlue(), panelConstraints);
        panelConstraints.weightx = 0;

        Image bombsImg = ImageUtil.getImage("img/icon.png");
        Image bombsNewImg = bombsImg != null ? bombsImg.getScaledInstance(EXTRA_ITEM_SIZE, EXTRA_ITEM_SIZE, Image.SCALE_SMOOTH) : null;
        JLabel bombsLabel = new JLabel(bombsNewImg != null ? new ImageIcon(bombsNewImg) : null);
        panelConstraints.gridx = 3;
        panelConstraints.gridy = 0;
        extraOptionsPanel.add(bombsLabel, panelConstraints);
        bombsText = new JLabel();
        bombsText.setOpaque(true);
        bombsText.setBackground(Color.WHITE);
        bombsText.setPreferredSize(new Dimension(EXTRA_ITEM_WIDTH, EXTRA_ITEM_HEIGHT));
        bombsText.setBorder(border);
        panelConstraints.gridx = 4;
        panelConstraints.gridy = 0;
        bombsText.setHorizontalAlignment(JTextField.CENTER);
        bombsText.setFont(new Font(Font.SERIF, Font.BOLD, 20));
        extraOptionsPanel.add(bombsText, panelConstraints);
        frameConstraints.insets = new Insets(10, 40, 10, 40);
        frameConstraints.gridx = 0;
        frameConstraints.gridy = 1;
        mainFrame.add(extraOptionsPanel, frameConstraints);
    }

    public void showViewWindow() {
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    private void initMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu jMenuGame = new JMenu(Strings.get("GAME"));
        jMenuItemNewGame = new JMenuItem(Strings.get("NEW_GAME"));
        jMenuItemHighscores = new JMenuItem(Strings.get("HIGHSCORES"));
        jMenuItemSettings = new JMenuItem(Strings.get("SETTINGS"));
        jMenuItemSettings.addActionListener(e -> settingsView.showSettingsView());
        jMenuItemExit = new JMenuItem(Strings.get("EXIT"));
        jMenuItemExit.addActionListener(e -> System.exit(0));
        jMenuGame.add(jMenuItemNewGame);
        jMenuGame.addSeparator();
        jMenuGame.add(jMenuItemHighscores);
        jMenuGame.add(jMenuItemSettings);
        jMenuGame.addSeparator();
        jMenuGame.add(jMenuItemExit);
        menuBar.add(jMenuGame);
        JMenu jMenuEnquiry = new JMenu(Strings.get("ENQUIRY"));
        aboutProgram = new JMenuItem(Strings.get("ABOUT_PROGRAM"));
        aboutProgram.addActionListener(e -> showAboutProgramView());
        jMenuEnquiry.add(aboutProgram);
        menuBar.add(jMenuEnquiry);
        mainFrame.setJMenuBar(menuBar);
    }

    private void setFieldView(FieldView fieldView) {
        if (this.fieldView != null) {
            mainFrame.remove(this.fieldView.getPanel());
        }
        frameConstraints.insets = new Insets(40, 40, 0, 40);
        frameConstraints.gridx = 0;
        frameConstraints.gridy = 0;
        mainFrame.add(fieldView.getPanel(), frameConstraints);
        this.fieldView = fieldView;
        mainFrame.revalidate();
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
    }

    public void closeWindowView() {
        mainFrame.setVisible(false);
    }

    public void destroyWindowView() {
        mainFrame.dispose();
    }

    public void addActionListenerToViewWindowMenuItemNewGame(ActionListener actionListener) {
        jMenuItemNewGame.addActionListener(actionListener);
    }

    public void addActionListenerToViewWindowMenuItemExit(ActionListener actionListener) {
        jMenuItemExit.addActionListener(actionListener);
    }

    public void addActionListenerToViewWindowMenuItemSettings(ActionListener actionListener) {
        jMenuItemSettings.addActionListener(actionListener);
    }

    public void addActionListenerToViewWindowMenuItemAboutProgram(ActionListener actionListener) {
        aboutProgram.addActionListener(actionListener);
    }

    public void addActionListenerToViewWindowMenuItemStatistics(ActionListener actionListener) {
        jMenuItemHighscores.addActionListener(actionListener);
    }

    private void addActionListenerToViewFieldButtons(MouseAdapter mouseAdapter) {
        for (int i = 0; i < fieldView.getRowsNum(); i++) {
            for (int j = 0; j < fieldView.getColsNum(); j++) {
                JButton button = fieldView.getButtons()[i][j];
                button.addMouseListener(mouseAdapter);
            }
        }
    }

    private void showWinMessageWin(Difficulty difficulty) {
        if (difficulty == Difficulty.SPECIAL) {
            JOptionPane.showMessageDialog(mainFrame, Strings.get("WIN_MESSAGE"), Strings.get("WIN"),
                    JOptionPane.INFORMATION_MESSAGE, new ImageIcon(winNewImg));
        } else {
            String message = Strings.get("WIN_MESSAGE") + System.lineSeparator() + Strings.get("ENTER_NAME_MESSAGE");
            String winnerName = (String) JOptionPane.showInputDialog(mainFrame, message, Strings.get("WIN"),
                    JOptionPane.INFORMATION_MESSAGE, new ImageIcon(winNewImg), null, "");
            if (winnerName == null || winnerName.isEmpty()) {
                return;
            }
            sendToObservers(winnerName);

        }
    }

    public void register(ViewListener outlet) {
        observers.add(outlet);
    }

    private void sendToObservers(String name) {
        for (ViewListener listener : this.observers) {
            listener.updateStatistics(name);
        }
    }

    private void sendToObservers(MouseEvent event) {
        for (ViewListener listener : this.observers) {
            listener.mousePressed(event);
        }
    }

    @Override
    public void updateModelState(ModelUpdateResult modelUpdateResult) {
        fieldView.setCurrentBoxesForModify(modelUpdateResult.getBoxes());
        fieldView.updateFieldView();
        bombsText.setText(String.valueOf(modelUpdateResult.getUnmarkedBombsNum()));
        if (modelUpdateResult.getGameState() == GameState.WIN) {
            showWinMessageWin(modelUpdateResult.getDifficulty());
        }
    }

    @Override
    public void updateStatistics(Map<Difficulty, List<Highscore>> statistics) {
        showStatisticsView(statistics);
    }

    @Override
    public void updateTimer(int timerValue) {
        timerText.setText(String.valueOf(timerValue));
    }

    @Override
    public void updateSettingsModel(SettingsModel modelSettings) {
        if(modelSettings.getAdoptionSettings()==AdoptionSettings.SETTINGS_CHANGED){
            FieldView newFieldView = new FieldView(modelSettings.getRows(), modelSettings.getCols(), BUTTON_SIZE);
            setFieldView(newFieldView);
            addActionListenerToViewFieldButtons(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    sendToObservers(e);
                }
            });
        }
    }
}


