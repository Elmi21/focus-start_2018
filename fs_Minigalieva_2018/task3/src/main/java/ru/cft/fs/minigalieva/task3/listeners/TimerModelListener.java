package ru.cft.fs.minigalieva.task3.listeners;

public interface TimerModelListener {
    void updateTimer(int value);
}
