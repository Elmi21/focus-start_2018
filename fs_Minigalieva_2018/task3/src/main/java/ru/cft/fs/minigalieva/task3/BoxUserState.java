package ru.cft.fs.minigalieva.task3;

public enum BoxUserState {
    OPENED,
    CLOSED,
    FLAGGED,
    BOMBED,
    NO_BOMB
}