package ru.cft.fs.minigalieva.task3.database;

import ru.cft.fs.minigalieva.task3.Difficulty;
import ru.cft.fs.minigalieva.task3.utils.JPAUtil;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class StatisticsRepository {
    private EntityManager entityManager;
    public StatisticsRepository(){
        entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
    }

    public void add(Highscore highscore){
        entityManager.getTransaction().begin();
        entityManager.persist(highscore);
        entityManager.getTransaction().commit();
    }

    public List<Highscore> getHighscores(Difficulty difficulty) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Highscore> criteria = criteriaBuilder.createQuery(Highscore.class);
        Root<Highscore> root = criteria.from(Highscore.class);
        criteria.select(root).where(criteriaBuilder.equal(root.get("difficulty"), difficulty)).orderBy
                (criteriaBuilder.asc(root.get("time")));
        return entityManager.createQuery(criteria).setMaxResults(10).getResultList();
    }
}
