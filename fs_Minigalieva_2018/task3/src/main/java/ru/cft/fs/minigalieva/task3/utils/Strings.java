package ru.cft.fs.minigalieva.task3.utils;

import java.util.ResourceBundle;

public final class Strings {

    private static final ResourceBundle STRINGS_BUNDLE = ResourceBundle.getBundle("Strings", new UTF8Control());

    private Strings() {
    }

    public static String get(String key) {
        return STRINGS_BUNDLE.getString(key);
    }

    public static String get(String key, Object... args) {
        return String.format(get(key), args);
    }
}


