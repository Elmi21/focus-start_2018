package ru.cft.fs.minigalieva.task3.model.game;

import ru.cft.fs.minigalieva.task3.*;
import ru.cft.fs.minigalieva.task3.database.Highscore;
import ru.cft.fs.minigalieva.task3.database.StatisticsRepository;
import ru.cft.fs.minigalieva.task3.listeners.GameListener;
import ru.cft.fs.minigalieva.task3.listeners.SettingsModelListener;
import ru.cft.fs.minigalieva.task3.model.settings.SettingsModel;
import ru.cft.fs.minigalieva.task3.model.timer.TimerModel;

import java.util.*;

public class Game implements SettingsModelListener {

    private GameField gameField;
    private int rows;
    private int cols;
    private int bombs;
    private GameState gameState;
    private List<Box> currentModifiedBoxes;
    private int unmarkedBombsNum;
    private ModelUpdateResult modelUpdateResult;
    private TimerModel timerModel;
    private List<GameListener> observers;
    private StatisticsRepository statisticsRepository;


    public Game(TimerModel timerModel) {
        this.modelUpdateResult = new ModelUpdateResult();
        this.timerModel = timerModel;
        observers = new ArrayList<>();
        statisticsRepository = new StatisticsRepository();
        Thread timerThread = new Thread(timerModel);
        timerThread.start();
        newGame();
    }

    private void newGame() {
        gameField = new GameField(rows, cols, bombs);
        restartGame();
    }

    public void restartGame() {
        gameField.setClosedBoxesNumber(rows * cols);
        timerModel.pause();
        timerModel.setStartTime(0);
        gameField.initField();
        Coordinate localCoordinate;
        currentModifiedBoxes = new ArrayList<>();
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                localCoordinate = new Coordinate(x, y);
                currentModifiedBoxes.add(new Box(gameField.getBoxGameState(localCoordinate),
                        gameField.getBoxUserState(localCoordinate), localCoordinate));
            }
        }

        gameState = GameState.PLAY;
        unmarkedBombsNum = bombs;
        sendToObservers(getModelUpdateResult());
    }

    public void tryToggleFlag(Coordinate coordinate) {
        currentModifiedBoxes = new ArrayList<>();
        switch (gameField.getBoxUserState(coordinate)) {
            case FLAGGED:
                setBox(coordinate, BoxUserState.CLOSED);
                unmarkedBombsNum++;
                break;
            case CLOSED:
                setBox(coordinate, BoxUserState.FLAGGED);
                unmarkedBombsNum--;
                break;
            default:
                break;
        }
        sendToObservers(getModelUpdateResult());
    }

    public void tryOpenBox(Coordinate coordinate) {
        if (timerModel.getStartTime() == 0) {
            timerModel.setStartTime(System.nanoTime());
            timerModel.resume();
        }
        if (checkDefeat()) return;
        currentModifiedBoxes = new ArrayList<>();
        openBox(coordinate);
        checkWin();
        sendToObservers(getModelUpdateResult());
    }

    private void openBox(Coordinate coordinate) {
        if (gameField.getBoxUserState(coordinate) == BoxUserState.CLOSED) {
            switch (gameField.getBoxGameState(coordinate)) {
                case ZERO:
                    openNeighbourBoxes(coordinate);
                    break;
                case BOMB:
                    if (gameField.getClosedBoxesNumber() == gameField.getAllCoordinatesNumber()) {
                        gameState = GameState.PLAY;
                        restartGame();
                        openBox(coordinate);
                    } else {
                        finishGame(coordinate);
                    }
                    break;
                default:
                    setBox(coordinate, BoxUserState.OPENED);
                    break;
            }
        }
    }

    private void openNeighbourBoxes(Coordinate coordinate) {
        setBox(coordinate, BoxUserState.OPENED);
        for (Coordinate around : gameField.getNeighbourBoxes(coordinate)) {
            openBox(around);
        }
    }

    private void finishGame(Coordinate coordinate) {
        gameState = GameState.DEFEAT;
        timerModel.pause();
        setBox(coordinate, BoxUserState.BOMBED);
        Coordinate localCoordinate;
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                localCoordinate = new Coordinate(x, y);
                if (gameField.getBoxGameState(localCoordinate) == BoxGameState.BOMB) {
                    if (gameField.getBoxUserState(localCoordinate) == BoxUserState.CLOSED) {
                        setBox(localCoordinate, BoxUserState.OPENED);
                    }
                } else {
                    if (gameField.getBoxUserState(localCoordinate) == BoxUserState.FLAGGED) {
                        setBox(localCoordinate, BoxUserState.NO_BOMB);
                    }
                }
            }
        }
    }

    public void tryOpenNeighbourBoxes(Coordinate coordinate) {
        currentModifiedBoxes = new ArrayList<>();
        if (checkDefeat() || gameField.getBoxUserState(coordinate) != BoxUserState.OPENED || gameField.getFlaggedNeighbourBoxesNumber
                (coordinate) != gameField.getBoxGameState(coordinate).getBoxState()) {
            return;
        }
        for (Coordinate localCoordinate : gameField.getNeighbourBoxes(coordinate)) {
            if (gameField.getBoxUserState(localCoordinate) == BoxUserState.CLOSED) {
                openBox(localCoordinate);
            }
        }
        checkWin();
        sendToObservers(getModelUpdateResult());
    }

    private void checkWin() {
        if ((gameState == GameState.PLAY) && (gameField.getClosedBoxesNumber() == bombs)) {
            gameState = GameState.WIN;
            timerModel.pause();
        }
    }

    private boolean checkDefeat() {
        return gameState != GameState.PLAY;
    }


    private void setBox(Coordinate coordinate, BoxUserState boxUserState) {
        if (gameField.isCoordinateInField(coordinate)) {
            gameField.setBoxUserState(coordinate, boxUserState);
            currentModifiedBoxes.add(new Box(gameField.getBoxGameState(coordinate),
                    gameField.getBoxUserState(coordinate), coordinate));
        }
    }

    private ModelUpdateResult getModelUpdateResult() {
        modelUpdateResult.setGameState(gameState);
        modelUpdateResult.setBoxes(currentModifiedBoxes);
        modelUpdateResult.setUnmarkedBombsNum(unmarkedBombsNum);
        return modelUpdateResult;
    }

    @Override
    public void updateSettingsModel(SettingsModel settingsModel) {
        this.rows = settingsModel.getRows();
        this.cols = settingsModel.getCols();
        this.bombs = settingsModel.getBombs();
        modelUpdateResult.setDifficulty(settingsModel.getDifficulty());
        newGame();
    }

    public void register(GameListener outlet) {
        observers.add(outlet);
        outlet.updateModelState(getModelUpdateResult());
    }

    private void sendToObservers(ModelUpdateResult modelUpdateResult) {
        for (GameListener outlet : this.observers) {
            outlet.updateModelState(modelUpdateResult);
        }
    }

    private void sendToObservers(Map<Difficulty, List<Highscore>> statisticsDictionary) {
        for (GameListener outlet : this.observers) {
            outlet.updateStatistics(statisticsDictionary);
        }
    }

    public void getStatistics() {
        Map<Difficulty, List<Highscore>> statisticsDictionary = new EnumMap<>(Difficulty.class);
        for (Difficulty difficulty : Difficulty.values()) {
            if (difficulty == Difficulty.SPECIAL) {
                continue;
            }
            statisticsDictionary.put(difficulty, statisticsRepository.getHighscores(difficulty));
        }

        sendToObservers(statisticsDictionary);
    }

    public void addRecordToStatistics(String winnerName) {
        Highscore highscore = new Highscore();
        highscore.setCreatedDate(new Date());
        highscore.setDifficulty(modelUpdateResult.getDifficulty());
        highscore.setName(winnerName);
        highscore.setTime(Integer.parseInt(String.valueOf(timerModel.getSecondEstimatedTime())));
        statisticsRepository.add(highscore);
    }
}
