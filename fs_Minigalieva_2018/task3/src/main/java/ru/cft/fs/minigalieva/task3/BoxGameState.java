package ru.cft.fs.minigalieva.task3;

public enum BoxGameState {
    ZERO,
    NUM1,
    NUM2,
    NUM3,
    NUM4,
    NUM5,
    NUM6,
    NUM7,
    NUM8,
    BOMB;

    public BoxGameState getNextBoxState() {
        return BoxGameState.values()[this.ordinal() + 1];
    }

    public int getBoxState() {
        return this.ordinal();
    }
}