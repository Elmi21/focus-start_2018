package ru.cft.fs.minigalieva.task3;

public enum GameState {
    PLAY,
    WIN,
    DEFEAT
}
