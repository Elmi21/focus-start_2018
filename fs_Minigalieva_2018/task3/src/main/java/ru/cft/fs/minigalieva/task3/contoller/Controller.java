package ru.cft.fs.minigalieva.task3.contoller;

import ru.cft.fs.minigalieva.task3.Coordinate;
import ru.cft.fs.minigalieva.task3.Difficulty;
import ru.cft.fs.minigalieva.task3.listeners.SettingsViewListener;
import ru.cft.fs.minigalieva.task3.listeners.ViewListener;
import ru.cft.fs.minigalieva.task3.model.game.Game;
import ru.cft.fs.minigalieva.task3.model.settings.SettingsModel;
import ru.cft.fs.minigalieva.task3.utils.JPAUtil;

import javax.swing.*;
import java.awt.event.*;

public class Controller implements ViewListener, SettingsViewListener {

    private Game game;
    private SettingsModel settingsModel;



    public Controller(Game game, SettingsModel settingsModel) {
        this.game=game;
        this.settingsModel=settingsModel;
        Runtime.getRuntime().addShutdownHook(new Thread(JPAUtil::shutdown));
    }

    public void restartGame(){
        game.restartGame();
    }

    public void showStatistics(){
        game.getStatistics();
    }
    public void mousePressed(MouseEvent e){
        int x = (Integer) ((JButton) e.getSource()).getClientProperty("x");
        int y = (Integer) ((JButton) e.getSource()).getClientProperty("y");
        Coordinate coordinate = new Coordinate(x, y);
        if (SwingUtilities.isLeftMouseButton(e)) {
            if (SwingUtilities.isRightMouseButton(e)) {
                game.tryOpenNeighbourBoxes(coordinate);
            } else {
                game.tryOpenBox(coordinate);
            }
        } else if (SwingUtilities.isRightMouseButton(e)) {
            game.tryToggleFlag(coordinate);
        } else if (SwingUtilities.isMiddleMouseButton(e)) {
            game.tryOpenNeighbourBoxes(coordinate);
        }
    }


    @Override
    public void updateStatistics(String winnerName) {
        game.addRecordToStatistics(winnerName);

    }

    @Override
    public void updateSettings(Difficulty difficulty, int height, int width, int bombs) {
        settingsModel.setSettings(difficulty, height, width, bombs);
    }
}