package ru.cft.fs.minigalieva.task3.view.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionListenerToViewWindowMenuItemExit implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}
