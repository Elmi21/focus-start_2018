package ru.cft.fs.minigalieva.task3.view.listeners;

import ru.cft.fs.minigalieva.task3.view.SettingsView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionListenerToViewWindowMenuItemSettings implements ActionListener {
    private SettingsView settingsView;
    public ActionListenerToViewWindowMenuItemSettings(SettingsView settingsView)
    {
        this.settingsView=settingsView;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        settingsView.showSettingsView();

    }
}
