package ru.cft.fs.minigalieva.task3.model.timer;

import ru.cft.fs.minigalieva.task3.listeners.TimerModelListener;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

public class TimerModel implements Runnable {
    private boolean running = true;
    private boolean paused = false;
    private long startTime;
    private static final Object pauseLock = new Object();
    private List<TimerModelListener> observers;
    private long secondEstimatedTime;

    public TimerModel() {
        observers = new ArrayList<>();
    }

    @Override
    public void run() {
        while (running) {
            synchronized (pauseLock) {
                if (running && paused) {
                    try {
                        pauseLock.wait();
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
                if (!running) {
                    break;
                }
            }
            long nanosecondEstimatedTime = System.nanoTime() - startTime;
            secondEstimatedTime = (long) Math.floor(nanosecondEstimatedTime / (1e9));
            sendToObservers((int) secondEstimatedTime);

            try {
                sleep(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();

            }

        }
    }

    public void stop() {
        running = false;
        Thread.currentThread().interrupt();
    }

    public void pause() {
        paused = true;
    }

    public void resume() {
        synchronized (pauseLock) {
            paused = false;
            pauseLock.notifyAll();
        }
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
        sendToObservers(0);
    }

    public long getStartTime() {
        return startTime;
    }

    public void register(TimerModelListener outlet) {
        observers.add(outlet);
    }

    private void sendToObservers(int value) {
        for (TimerModelListener outlet : this.observers) {
            outlet.updateTimer(value);
        }
    }

    public long getSecondEstimatedTime() {
        return secondEstimatedTime;
    }
}
