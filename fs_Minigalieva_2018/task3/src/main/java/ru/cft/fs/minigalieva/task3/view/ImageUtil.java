package ru.cft.fs.minigalieva.task3.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;

class ImageUtil {
    private static final Logger logger = LoggerFactory.getLogger(ImageUtil.class);
    private ImageUtil(){

    }
    static Image getImage(String filePath) {
        try {
            return ImageIO.read(ImageUtil.class.getResource(filePath));
        } catch (IOException e) {
            logger.error("Resource is not found");
            return null;
        }
    }
}
