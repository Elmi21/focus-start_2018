package ru.cft.fs.minigalieva.task3.listeners;

import ru.cft.fs.minigalieva.task3.Difficulty;

public interface SettingsViewListener {
    void updateSettings(Difficulty difficulty, int height, int width, int bombs);
}
