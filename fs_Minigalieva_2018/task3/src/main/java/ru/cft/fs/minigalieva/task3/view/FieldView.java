package ru.cft.fs.minigalieva.task3.view;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

import ru.cft.fs.minigalieva.task3.Box;
import ru.cft.fs.minigalieva.task3.BoxGameState;
import ru.cft.fs.minigalieva.task3.BoxUserState;

class FieldView {

    private JPanel panel;
    private int colsNum;
    private int rowsNum;
    private int buttonSize;
    private JButton[][] buttons;
    private List<Box> currentBoxesForModify;
    private Map<String, Image> imageDictionary;

    FieldView(int rowsNum, int colsNum, int buttonSize) {
        this.colsNum = colsNum;
        this.rowsNum = rowsNum;
        this.buttonSize = buttonSize;
        buttons = new JButton[rowsNum][colsNum];
        initField();
        imageDictionary  = new HashMap<>();
        for (BoxGameState boxGameState : BoxGameState.values()) {
            initImageDictionary(boxGameState.toString());
        }
        for (BoxUserState boxUserState : BoxUserState.values()) {
            initImageDictionary(boxUserState.toString());
        }
    }

    private void initImageDictionary(String str){
        Image img = ImageUtil.getImage("img/" + str + ".png");
        Image newImg = img != null ? img.getScaledInstance(buttonSize, buttonSize, Image.SCALE_SMOOTH) : null;
        imageDictionary.put(str, newImg);
    }
    private void initField() {
        panel = new JPanel();
        GridLayout gridLayout = new GridLayout(rowsNum, colsNum);
        panel.setLayout(gridLayout);
        for (int i = 0; i < rowsNum; i++) {
            for (int j = 0; j < colsNum; j++)
            {
                JButton button = new JButton();
                button.putClientProperty("x", i);
                button.putClientProperty("y", j);
                button.setPreferredSize(new Dimension(buttonSize, buttonSize));
                buttons[i][j]=button;
                panel.add(button);
            }
        }
    }

    void updateFieldView() {
        for (Box box : currentBoxesForModify) {
            ImageIcon icon = new ImageIcon(imageDictionary.get(box.getCurrentBoxState()));
            buttons[box.getCoordinate().getX()][box.getCoordinate().getY()].setIcon(icon);
        }
    }

    JPanel getPanel() {
        return panel;
    }

    void setCurrentBoxesForModify(List<Box> currentBoxesForModify) {
        this.currentBoxesForModify = currentBoxesForModify;
    }

    JButton[][] getButtons() {
        return buttons;
    }

    int getColsNum() {
        return colsNum;
    }

    int getRowsNum() {
        return rowsNum;
    }
}