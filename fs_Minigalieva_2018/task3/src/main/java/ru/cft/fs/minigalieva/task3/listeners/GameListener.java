package ru.cft.fs.minigalieva.task3.listeners;

import ru.cft.fs.minigalieva.task3.Difficulty;
import ru.cft.fs.minigalieva.task3.database.Highscore;
import ru.cft.fs.minigalieva.task3.ModelUpdateResult;

import java.util.List;
import java.util.Map;

public interface GameListener {
    void updateModelState(ModelUpdateResult modelUpdateResult);
    void updateStatistics(Map<Difficulty, List<Highscore>> statistics);
}
