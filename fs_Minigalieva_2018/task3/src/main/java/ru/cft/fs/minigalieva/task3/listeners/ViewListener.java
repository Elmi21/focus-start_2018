package ru.cft.fs.minigalieva.task3.listeners;

import java.awt.event.MouseEvent;

public interface ViewListener {
    void updateStatistics(String name);
    void mousePressed(MouseEvent event);
}
