package ru.cft.fs.minigalieva.task3;

import java.util.List;

public class ModelUpdateResult {

    private List<Box> boxes;
    private GameState gameState;
    private int unmarkedBombsNum;
    private Difficulty difficulty;

    public List<Box> getBoxes() {
        return boxes;
    }

    public void setBoxes(List<Box> boxes) {
        this.boxes = boxes;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public int getUnmarkedBombsNum() {
        return unmarkedBombsNum;
    }

    public void setUnmarkedBombsNum(int unmarkedBombsNum) {
        this.unmarkedBombsNum = unmarkedBombsNum;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }
}
