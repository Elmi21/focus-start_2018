package ru.cft.fs.minigalieva.task2.metrics;

public final class Format {
    public static final String NAME_FORMAT = "%s: %s%n";
    public static final String STRING_FORMAT = "%s: %.2f %s%n";
    public static final String ANGLE_STRING_FORMAT = "%s: %.2f%s%n";

    private Format() {

    }
}
