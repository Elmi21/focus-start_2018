package ru.cft.fs.minigalieva.task2.exceptions;

public class IncorrectFigureParametersException extends Exception {
    public IncorrectFigureParametersException(String figure) {
        super(String.format("Incorrect parameters for %s", figure));
    }
}
