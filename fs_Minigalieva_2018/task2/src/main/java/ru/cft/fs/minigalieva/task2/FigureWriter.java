package ru.cft.fs.minigalieva.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.fs.minigalieva.task2.views.View;

import java.io.*;

public class FigureWriter implements AutoCloseable {

    private final OutputStream stream;
    private final OutputStreamWriter writer;
    private static final Logger logger = LoggerFactory.getLogger(FigureWriter.class);

    FigureWriter(String filePath) throws FileNotFoundException {
        stream = filePath != null ? new FileOutputStream(filePath) : System.out;
        writer = new OutputStreamWriter(stream);
    }

    public void writeFigure(View view) throws IOException {
        try {
            view.printTo(writer);
            writer.flush();
        } catch (IOException e) {
            logger.error(Messages.get("FILE_WRITING_ERROR"));
            throw e;
        }
    }

    @Override
    public void close() throws IOException {
        if (stream instanceof PrintStream) {
            writer.flush();
        } else {
            writer.close();
        }
    }
}
