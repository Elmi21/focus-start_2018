package ru.cft.fs.minigalieva.task2;

import ru.cft.fs.minigalieva.task2.exceptions.*;
import ru.cft.fs.minigalieva.task2.figures.*;
import ru.cft.fs.minigalieva.task2.views.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


class Task2 {
    private static final Logger logger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    /**
     * Точка входа в приложение
     *
     * @param consoleParameters входные параметры
     *                          1 - имя входного файла
     *                          2 - имя выходного файла (если не указано, информация выводится в консоль)
     */
    public static void main(String[] consoleParameters) {
        if (consoleParameters.length == 0) {
            logger.error(Messages.get("CONSOLE_PARAMETERS_WERE_NOT_ENTERED"));
            return;
        }
        if (consoleParameters.length > 2) {
            logger.error(Messages.get("WRONG_NUMBER_OF_CONSOLE_PARAMETERS"));
            return;
        }
        String inputPath = consoleParameters[0];
        String outputPath = consoleParameters.length > 1 ? consoleParameters[1] : null;
        try {
            process(inputPath, outputPath);
        } catch (FigureReaderException e) {
            logger.error(e.getMessage(), e);
        } catch (Exception e) {
            logger.error(Messages.get("ERROR_MESSAGE"), e);
        }
    }

    private static void process(String inputPath, String outputPath) throws IOException, FigureReaderException,
            UnknownFigureException {
        try (FigureReader reader = new FigureReader(inputPath);
             FigureWriter writer = new FigureWriter(outputPath)) {
            Figure figure;
            while ((figure = reader.readFigure()) != null) {
                writer.writeFigure(createView(figure));
            }
            logger.info(Messages.get("FILE_PROCESSING_IS_FINISHED"));
        }
    }

    private static View createView(Figure figure) throws UnknownFigureException {
        switch (figure.getClass().getSimpleName()) {
            case Circle.NAME:
                return new CircleView((Circle) figure);
            case Rectangle.NAME:
                return new RectangleView((Rectangle) figure);
            case Triangle.NAME:
                return new TriangleView((Triangle) figure);
            default:
                throw new UnknownFigureException();
        }
    }
}