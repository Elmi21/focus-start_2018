package ru.cft.fs.minigalieva.task2.figures;

public interface Figure {

    String getPrintName();

    double getArea();

    double getPerimeter();
}
