package ru.cft.fs.minigalieva.task2.exceptions;

/**
 * исключение с информацией о том, в какой строке было проиведено ислючение
 */
public class FigureReaderException extends Exception {
    public FigureReaderException(int line, Throwable exception) {
        super(String.format("Error in line %d: %s", line, exception.getMessage()), exception);
    }
}
