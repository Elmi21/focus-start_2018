package ru.cft.fs.minigalieva.task2.metrics;

public final class Unit {
    public static final String SQUARE_MILLIMETERS = "кв. мм";
    public static final String MILLIMETERS = "мм";
    public static final String DEGREES = "°";

    private Unit() {

    }
}
