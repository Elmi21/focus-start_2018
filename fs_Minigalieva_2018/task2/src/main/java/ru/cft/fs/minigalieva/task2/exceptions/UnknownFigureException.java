package ru.cft.fs.minigalieva.task2.exceptions;

import ru.cft.fs.minigalieva.task2.Messages;

public class UnknownFigureException extends Exception {
    public UnknownFigureException() {
        super(Messages.get("UNKNOWN_FIGURE"));
    }
}