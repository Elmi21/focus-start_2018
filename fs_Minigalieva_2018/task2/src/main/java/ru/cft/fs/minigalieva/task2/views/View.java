package ru.cft.fs.minigalieva.task2.views;

import ru.cft.fs.minigalieva.task2.figures.Figure;
import ru.cft.fs.minigalieva.task2.metrics.Characteristic;
import ru.cft.fs.minigalieva.task2.metrics.Format;
import ru.cft.fs.minigalieva.task2.metrics.Unit;

import java.io.IOException;
import java.io.OutputStreamWriter;


public abstract class View<T extends Figure> {
    protected T figure;

    View(T figure) {
        this.figure = figure;
    }

    public void printTo(OutputStreamWriter writer)throws IOException{
        printGeneralCharacteristics(writer);
        printIndividualCharacteristics(writer);
        writer.write(System.lineSeparator());
    }
    private void printGeneralCharacteristics(OutputStreamWriter writer) throws IOException {
        writer.write(String.format(Format.NAME_FORMAT, Characteristic.FIGURE, figure.getPrintName()));
        writer.write(String.format(Format.STRING_FORMAT, Characteristic.AREA, figure.getArea(), Unit
                .SQUARE_MILLIMETERS));
        writer.write(String.format(Format.STRING_FORMAT, Characteristic.PERIMETER, figure.getPerimeter(), Unit.MILLIMETERS));
    }

    public abstract void printIndividualCharacteristics(OutputStreamWriter writer) throws IOException;

    protected T getFigure() {
        return this.figure;
    }
}
