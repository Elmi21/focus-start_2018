package ru.cft.fs.minigalieva.task2.figures;

import static java.lang.Math.*;

public class Triangle implements Figure {
    public static final String NAME = "Triangle";
    private final double side1;
    private final double side2;
    private final double side3;

    public Triangle(double side1, double side2, double side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    private double cosA(double sideA, double sideB, double sideC) {
        return (sideB * sideB + sideC * sideC - sideA * sideA) / (2. * sideB * sideC);
    }

    @Override
    public String getPrintName() {
        return "Треугольник";
    }

    @Override
    public double getArea() {
        double p = 0.5 * (side1 + side2 + side3);
        return sqrt(p * (p - side1) * (p - side2) * (p - side3));
    }

    @Override
    public double getPerimeter() {
        return side1 + side2 + side3;
    }

    public double getSide1() {
        return side1;
    }

    public double getSide2() {
        return side2;
    }

    public double getSide3() {
        return side3;
    }

    public double getAngle1() {
        return toDegrees(acos(cosA(side1, side2, side3)));
    }

    public double getAngle2() {
        return toDegrees(acos(cosA(side2, side1, side3)));
    }

    public double getAngle3() {
        return toDegrees(acos(cosA(side3, side1, side2)));
    }
}
