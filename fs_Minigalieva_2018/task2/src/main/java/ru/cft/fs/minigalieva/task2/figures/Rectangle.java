package ru.cft.fs.minigalieva.task2.figures;

public class Rectangle implements Figure {
    public static final String NAME = "Rectangle";
    private final double side1;
    private final double side2;

    public Rectangle(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    @Override
    public String getPrintName() {
        return "Прямоугольник";
    }

    @Override
    public double getArea() {
        return side1 * side2;
    }

    @Override
    public double getPerimeter() {
        return 2 * (side1 + side2);
    }

    public double getLength() {
        return Math.max(side1, side2);
    }

    public double getDiagonal() {
        return Math.sqrt(side1 * side1 + side2 * side2);
    }

    public double getWidth() {
        return Math.min(side1, side2);
    }
}
