package ru.cft.fs.minigalieva.task2.metrics;

public final class Characteristic {

    public static final String FIGURE = "Фигура";
    public static final String AREA = "Площадь";
    public static final String PERIMETER = "Периметр";
    public static final String RADIUS = "Радиус";
    public static final String DIAMETER = "Диаметр";
    public static final String SIDE = "Сторона";
    public static final String ANGLE = "Угол";
    public static final String LENGTH = "Длина";
    public static final String WIDTH = "Ширина";
    public static final String DIAGONAL = "Диагональ";

    private Characteristic() {

    }

}
