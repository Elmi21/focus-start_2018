package ru.cft.fs.minigalieva.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.fs.minigalieva.task2.exceptions.FigureReaderException;
import ru.cft.fs.minigalieva.task2.exceptions.UnknownFigureException;
import ru.cft.fs.minigalieva.task2.figures.Figure;

import java.io.*;

public class FigureReader implements AutoCloseable {
    private final BufferedReader reader;
    private int lineCounter = 0;
    private final Logger logger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    FigureReader(String filePath) throws FileNotFoundException {
        reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
    }

    public Figure readFigure() throws IOException, FigureReaderException {

        try {
            String name = reader.readLine();
            lineCounter++;
            if (name == null || name.isEmpty()) {
                return null;
            }
            if (!FigureParser.parseName(name)) {
                throw new UnknownFigureException();
            }
            String params = reader.readLine();
            lineCounter++;
            return FigureParser.parse(name, params);
        } catch (IOException e) {
            logger.error(Messages.get("FILE_READING_ERROR"));
            throw e;
        } catch (Exception e) {
            throw new FigureReaderException(lineCounter, e);
        }

    }

    @Override
    public void close() throws IOException {
        reader.close();
    }
}

