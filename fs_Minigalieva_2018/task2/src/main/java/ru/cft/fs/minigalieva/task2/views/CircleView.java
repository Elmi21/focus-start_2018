package ru.cft.fs.minigalieva.task2.views;

import ru.cft.fs.minigalieva.task2.figures.Circle;
import ru.cft.fs.minigalieva.task2.metrics.Characteristic;
import ru.cft.fs.minigalieva.task2.metrics.Format;
import ru.cft.fs.minigalieva.task2.metrics.Unit;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class CircleView extends View<Circle> {

    public CircleView(Circle circle) {
        super(circle);
        this.figure = circle;
    }

    public void printIndividualCharacteristics(OutputStreamWriter writer) throws IOException {
        writer.write(String.format(Format.STRING_FORMAT, Characteristic.RADIUS, figure.getRadius(), Unit
                .MILLIMETERS));
        writer.write(String.format(Format.STRING_FORMAT, Characteristic.DIAMETER, figure
                .getDiameter(), Unit.MILLIMETERS));
    }
}
