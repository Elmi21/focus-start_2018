package ru.cft.fs.minigalieva.task2;

import java.util.ResourceBundle;

public final class Messages {

    private static final ResourceBundle MESSAGES_BUNDLE = ResourceBundle.getBundle("MessagesBundle");

    private Messages() {

    }

    public static String get(String key) {
        return MESSAGES_BUNDLE.getString(key);
    }

}

