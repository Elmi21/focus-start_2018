package ru.cft.fs.minigalieva.task2.figures;

public class Circle implements Figure {
    public static final String NAME = "Circle";
    private final double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public String getPrintName() {
        return "Круг";
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * 2 * radius;
    }

    public double getDiameter() {
        return 2 * radius;
    }

    public double getRadius() {
        return radius;
    }
}
