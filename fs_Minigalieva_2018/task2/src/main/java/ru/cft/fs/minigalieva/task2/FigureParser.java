package ru.cft.fs.minigalieva.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.fs.minigalieva.task2.exceptions.IncorrectFigureParametersException;
import ru.cft.fs.minigalieva.task2.exceptions.UnknownFigureException;
import ru.cft.fs.minigalieva.task2.figures.*;

public final class FigureParser {

    private static final int TRIANGLE_PARAMETERS_NUMBER = 3;
    private static final int RECTANGLE_PARAMETERS_NUMBER = 2;
    private static final Logger logger = LoggerFactory.getLogger(FigureParser.class);

    private FigureParser() {

    }

    public static boolean parseName(String name) {
        return (name.equalsIgnoreCase(Circle.NAME) ||
                name.equalsIgnoreCase(Rectangle.NAME) ||
                name.equalsIgnoreCase(Triangle.NAME));
    }

    public static Figure parse(String name, String params) throws IncorrectFigureParametersException, UnknownFigureException {
        try {
            if (params == null || params.isEmpty()) {
                throw new IncorrectFigureParametersException(name);
            }
            if (name.equalsIgnoreCase(Circle.NAME)) {
                return parseCircle(params);
            } else if (name.equalsIgnoreCase(Rectangle.NAME)) {
                return parseRectangle(params);
            } else if (name.equalsIgnoreCase(Triangle.NAME)) {
                return parseTriangle(params);
            } else {
                throw new UnknownFigureException();
            }
        } catch (NumberFormatException e) {
            throw new IncorrectFigureParametersException(name);
        }
    }

    private static Circle parseCircle(String params) throws IncorrectFigureParametersException {
        double radius = Double.parseDouble(params);
        if (radius <= 0) {
            throw new IncorrectFigureParametersException(Circle.NAME);
        }
        logger.info(Messages.get("ADD_CIRCLE"), radius);
        return new Circle(radius);
    }

    private static Rectangle parseRectangle(String params) throws IncorrectFigureParametersException {
        String[] strArr;
        double[] numArr;
        strArr = params.split(" ");
        if (strArr.length != RECTANGLE_PARAMETERS_NUMBER) {
            throw new IncorrectFigureParametersException(Rectangle.NAME);
        }
        numArr = new double[strArr.length];
        for (int j = 0; j < strArr.length; j++) {
            numArr[j] = Double.parseDouble(strArr[j]);
            if (numArr[j] <= 0) {
                throw new IncorrectFigureParametersException(Rectangle.NAME);
            }
        }
        logger.info(Messages.get("ADD_RECTANGLE"), numArr[0], numArr[1]);
        return new Rectangle(numArr[0], numArr[1]);
    }

    private static Triangle parseTriangle(String params) throws IncorrectFigureParametersException {
        String[] strArr;
        double[] numArr;
        strArr = params.split(" ");
        if (strArr.length != TRIANGLE_PARAMETERS_NUMBER) {
            throw new IncorrectFigureParametersException(Triangle.NAME);
        }
        numArr = new double[strArr.length];
        for (int j = 0; j < strArr.length; j++) {
            numArr[j] = Double.parseDouble(strArr[j]);
            if (numArr[j] <= 0) {
                throw new IncorrectFigureParametersException(Triangle.NAME);
            }
        }
        if ((numArr[0] < numArr[1] + numArr[2]) &&
                (numArr[1] < numArr[2] + numArr[0]) &&
                (numArr[2] < numArr[0] + numArr[1])) {
            logger.info(Messages.get("ADD_TRIANGLE"), numArr[0], numArr[1], numArr[2]);
            return new Triangle(numArr[0], numArr[1], numArr[2]);
        } else {
            throw new IncorrectFigureParametersException(Triangle.NAME);
        }
    }

}

