package ru.cft.fs.minigalieva.task2.views;

import ru.cft.fs.minigalieva.task2.figures.Rectangle;
import ru.cft.fs.minigalieva.task2.metrics.Characteristic;
import ru.cft.fs.minigalieva.task2.metrics.Format;
import ru.cft.fs.minigalieva.task2.metrics.Unit;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class RectangleView extends View<Rectangle> {

    public RectangleView(Rectangle rectangle) {
        super(rectangle);
        this.figure = rectangle;
    }

    @Override
    public void printIndividualCharacteristics(OutputStreamWriter writer) throws IOException {
        writer.write(String.format(Format.STRING_FORMAT, Characteristic.DIAGONAL, figure.getDiagonal(), Unit
                .MILLIMETERS));
        writer.write(String.format(Format.STRING_FORMAT, Characteristic.LENGTH, figure.getLength(), Unit
                .MILLIMETERS));
        writer.write(String.format(Format.STRING_FORMAT, Characteristic.WIDTH, figure.getWidth(), Unit
                .MILLIMETERS));
    }
}
