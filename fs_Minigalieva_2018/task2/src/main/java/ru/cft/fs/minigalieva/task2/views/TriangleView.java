package ru.cft.fs.minigalieva.task2.views;

import ru.cft.fs.minigalieva.task2.figures.Triangle;
import ru.cft.fs.minigalieva.task2.metrics.Characteristic;
import ru.cft.fs.minigalieva.task2.metrics.Format;
import ru.cft.fs.minigalieva.task2.metrics.Unit;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class TriangleView extends View<Triangle> {

    public TriangleView(Triangle triangle) {
        super(triangle);
        this.figure = triangle;
    }

    @Override
    public void printIndividualCharacteristics(OutputStreamWriter writer) throws IOException {
        writer.write(String.format(Format.STRING_FORMAT, Characteristic.SIDE, figure.getSide1(), Unit
                .MILLIMETERS));
        writer.write(String.format(Format.ANGLE_STRING_FORMAT, Characteristic.ANGLE, figure
                .getAngle1(), Unit.DEGREES));
        writer.write(String.format(Format.STRING_FORMAT, Characteristic.SIDE, figure.getSide2(), Unit
                .MILLIMETERS));
        writer.write(String.format(Format.ANGLE_STRING_FORMAT, Characteristic.ANGLE, figure.getAngle2(),
                Unit.DEGREES));
        writer.write(String.format(Format.STRING_FORMAT, Characteristic.SIDE, figure.getSide3(), Unit
                .MILLIMETERS));
        writer.write(String.format(Format.ANGLE_STRING_FORMAT, Characteristic.ANGLE, figure
                .getAngle3(), Unit.DEGREES));
    }
}
