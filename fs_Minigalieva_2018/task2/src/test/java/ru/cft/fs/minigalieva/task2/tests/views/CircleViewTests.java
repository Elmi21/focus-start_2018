package ru.cft.fs.minigalieva.task2.tests.views;

import org.junit.Test;
import ru.cft.fs.minigalieva.task2.figures.Circle;
import ru.cft.fs.minigalieva.task2.views.CircleView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import static junit.framework.TestCase.assertEquals;

public class CircleViewTests {
    private CircleView circleView = new CircleView(new Circle(4));
    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final OutputStreamWriter writer = new OutputStreamWriter(out);

    @Test
    public void testPrintGeneralCharacteristics() throws IOException {
        circleView.printIndividualCharacteristics(writer);
        writer.flush();
        String result = String.format("Радиус: 4,00 мм%nДиаметр: 8,00 мм%n");
        assertEquals(result, out.toString());
    }

    @Test
    public void testPrintTo() throws IOException {
        circleView.printTo(writer);
        writer.flush();
        String result = String.format("Фигура: Круг%nПлощадь: 50,27 кв. мм%nПериметр: 25,13 мм%nРадиус: 4,00 " +
                "мм%nДиаметр: 8,00 мм%n%n");
        assertEquals(result, out.toString());
    }
}
