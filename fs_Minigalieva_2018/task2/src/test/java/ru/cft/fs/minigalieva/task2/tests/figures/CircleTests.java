package ru.cft.fs.minigalieva.task2.tests.figures;

import org.junit.Test;
import ru.cft.fs.minigalieva.task2.figures.Circle;

import static junit.framework.TestCase.assertEquals;

public class CircleTests {
    private Circle circle = new Circle(5);
    private int delta = 3;

    @Test
    public void testCircleGetPerimeter() {
        assertEquals(31.415, circle.getPerimeter(), delta);
    }

    @Test
    public void testCircleGetArea() {
        assertEquals(78.539, circle.getArea(), delta);
    }

    @Test
    public void testCircleGetRadius() {
        assertEquals(5, circle.getRadius(), delta);
    }

    @Test
    public void testCircleGetDiameter() {
        assertEquals(10, circle.getDiameter(), delta);
    }

    @Test
    public void testCircleGetPrintName() {
        assertEquals("Круг", circle.getPrintName());
    }
}
