package ru.cft.fs.minigalieva.task2.tests.views;

import org.junit.Test;
import ru.cft.fs.minigalieva.task2.figures.Triangle;
import ru.cft.fs.minigalieva.task2.views.TriangleView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import static junit.framework.TestCase.assertEquals;

public class TriangleViewTests {
    private TriangleView triangleView = new TriangleView(new Triangle(3, 4, 5));
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final OutputStreamWriter writer = new OutputStreamWriter(outContent);

    @Test
    public void testPrintGeneralCharacteristics() throws IOException {
        triangleView.printIndividualCharacteristics(writer);
        writer.flush();
        String result = String.format("Сторона: 3,00 мм%nУгол: 36,87°%nСторона: 4,00 мм%nУгол: 53,13°%nСторона: 5,00 мм%n" +
                "Угол: 90,00°%n");
        assertEquals(result, outContent.toString());
    }

    @Test
    public void testPrintTo() throws IOException {
        triangleView.printTo(writer);
        writer.flush();
        String result = String.format("Фигура: Треугольник%nПлощадь: 6,00 кв. мм%nПериметр: 12,00 мм%nСторона: 3,00 мм%n" +
                "Угол: 36,87°%nСторона: 4,00 мм%nУгол: 53,13°%nСторона: 5,00 мм%nУгол: 90,00°%n%n");
        assertEquals(result, outContent.toString());
    }

}
