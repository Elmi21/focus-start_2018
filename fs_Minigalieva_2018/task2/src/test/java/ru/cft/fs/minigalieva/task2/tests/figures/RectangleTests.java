package ru.cft.fs.minigalieva.task2.tests.figures;

import org.junit.Test;
import ru.cft.fs.minigalieva.task2.figures.Rectangle;

import static junit.framework.TestCase.assertEquals;

public class RectangleTests {
    private Rectangle rectangle = new Rectangle(10.5, 20.6);
    private int delta = 3;
    @Test
    public void testRectangleGetPerimeter() {
        assertEquals(62.2, rectangle.getPerimeter(), delta);
    }
    @Test
    public void testRectangleGetArea() {
        assertEquals(216.3, rectangle.getArea(), delta);
    }
    @Test
    public void testRectangleGetLength() {
        assertEquals(20.6, rectangle.getLength(), delta);
    }
    @Test
    public void testRectangleGetWidth() {
        assertEquals(10.5, rectangle.getWidth(), delta);
    }

    @Test
    public void testRectangleGetPrintName() {
        assertEquals("Прямоугольник", rectangle.getPrintName());
    }

    @Test
    public void testRectangleGetDiagonal() {
        assertEquals(23.121, rectangle.getDiagonal(), delta);
    }
}
