package ru.cft.fs.minigalieva.task2.tests;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Test;
import ru.cft.fs.minigalieva.task2.FigureParser;
import ru.cft.fs.minigalieva.task2.exceptions.IncorrectFigureParametersException;
import ru.cft.fs.minigalieva.task2.exceptions.UnknownFigureException;
import ru.cft.fs.minigalieva.task2.figures.Circle;
import ru.cft.fs.minigalieva.task2.figures.Figure;
import ru.cft.fs.minigalieva.task2.figures.Rectangle;
import ru.cft.fs.minigalieva.task2.figures.Triangle;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class FigureParserTests {

    private static final String RECTANGLE = "Rectangle";
    private static final String CIRCLE = "Circle";
    private static final String TRIANGLE = "Triangle";

    @Test
    public void testParseNameCircle() {
        boolean result = FigureParser.parseName(CIRCLE);
        assertEquals(true, result);
    }

    @Test
    public void testParseNameRectangle() {
        boolean result = FigureParser.parseName(RECTANGLE);
        assertEquals(true, result);
    }

    @Test
    public void testParseNameTriangle() {
        boolean result = FigureParser.parseName(TRIANGLE);
        assertEquals(true, result);
    }

    @Test
    public void testParseNameFalseName() {
        boolean result = FigureParser.parseName("Rhombus");
        assertEquals(false, result);
    }

    @Test
    public void testParseCircle() throws IncorrectFigureParametersException, UnknownFigureException {
        Figure figure = FigureParser.parse(CIRCLE, "8");
        assertTrue(EqualsBuilder.reflectionEquals(new Circle(8), figure));
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseCircleIncorrectParameter() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(CIRCLE, "we");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseCircleIncorrectNumberParameter() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(CIRCLE, "5 6");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseCircleIncorrectParameterWriting() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(CIRCLE, "5,");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseCircleIncorrectParameterZeroRadius() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(CIRCLE, "0");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseCircleIncorrectParameterNegativeRadius() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(CIRCLE, "-2");
    }

    @Test
    public void testParseRectangle() throws IncorrectFigureParametersException, UnknownFigureException {
        Figure figure = FigureParser.parse(RECTANGLE, "8 9");
        assertTrue(EqualsBuilder.reflectionEquals(new Rectangle(8, 9), figure));
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseRectangleIncorrectParameters() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(RECTANGLE, "z 9");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseRectangleIncorrectNumberParameters() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(RECTANGLE, "5");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseRectangleIncorrectParametersWriting() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(RECTANGLE, "5,6");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseRectangleIncorrectParametersZeroSide() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(RECTANGLE, "5 0");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseRectangleIncorrectParametersNegativeSide() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(RECTANGLE, "-9 8");
    }

    @Test
    public void testParseTriangle() throws IncorrectFigureParametersException, UnknownFigureException {
        Figure figure = FigureParser.parse(TRIANGLE, "3 4 5");
        assertTrue(EqualsBuilder.reflectionEquals(new Triangle(3, 4, 5), figure));
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseTriangleIncorrectParameters() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(TRIANGLE, "z 9 10");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseTriangleIncorrectNumberParameters() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(TRIANGLE, "5 9 6 3");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseFalseTriangleIncorrectParametersWriting() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(TRIANGLE, "3,4 5");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseTriangleIncorrectParametersZeroSide() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(TRIANGLE, "0 9 6");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseFalseTriangleIncorrectParametersNegativeSide() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(TRIANGLE, "3 4 -5");
    }

    @Test(expected = IncorrectFigureParametersException.class)
    public void testParseTriangleNotExist() throws IncorrectFigureParametersException,
            UnknownFigureException {
        FigureParser.parse(TRIANGLE, "3 4 1");
    }

}

