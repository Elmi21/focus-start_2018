package ru.cft.fs.minigalieva.task2.tests.views;

import org.junit.Test;
import ru.cft.fs.minigalieva.task2.figures.Rectangle;
import ru.cft.fs.minigalieva.task2.views.RectangleView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import static junit.framework.TestCase.assertEquals;

public class RectangleViewTests {
    private RectangleView rectangleView = new RectangleView(new Rectangle(4, 9));
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final OutputStreamWriter writer = new OutputStreamWriter(outContent);

    @Test
    public void testPrintGeneralCharacteristics() throws IOException {
        rectangleView.printIndividualCharacteristics(writer);
        writer.flush();
        String result = String.format("Диагональ: 9,85 мм%nДлина: 9,00 мм%nШирина: 4,00 мм%n");
        assertEquals(result, outContent.toString());
    }

    @Test
    public void testPrintTo() throws IOException {
        rectangleView.printTo(writer);
        writer.flush();
        String result = String.format("Фигура: Прямоугольник%nПлощадь: 36,00 кв. мм%nПериметр: 26,00 мм%nДиагональ: " +
                "9," +
                "85 мм%nДлина: 9,00 мм%nШирина: 4,00 мм%n%n");
        assertEquals(result, outContent.toString());
    }
}
