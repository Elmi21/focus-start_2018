package ru.cft.fs.minigalieva.task2.tests.figures;

import org.junit.Test;
import ru.cft.fs.minigalieva.task2.figures.Triangle;

import static junit.framework.TestCase.assertEquals;

public class TriangleTests {
    private Triangle triangle = new Triangle(6.8,6.8,10.8);
    private int delta = 2;
    @Test
    public void testRectangleGetSide1() {
        assertEquals(6.8, triangle.getSide1(), delta);
    }

    @Test
    public void testRectangleGetSide2() {
        assertEquals(6.8, triangle.getSide2(), delta);
    }

    @Test
    public void testRectangleGetSide3() {
        assertEquals(10.8, triangle.getSide3(), delta);
    }

    @Test
    public void testRectangleGetAngle1() {
        assertEquals(37.43, triangle.getAngle1(), delta);
    }

    @Test
    public void testRectangleGetAngle2() {
        assertEquals(37.43, triangle.getAngle2(), delta);
    }

    @Test
    public void testRectangleGetAngle3() {
        assertEquals(105.14, triangle.getAngle3(), delta);
    }

    @Test
    public void testRectangleGetArea() {
        assertEquals(22.32, triangle.getArea(), delta);
    }

    @Test
    public void testRectangleGetPerimeter() {
        assertEquals(24.4, triangle.getPerimeter(), delta);
    }

    @Test
    public void testRectangleGetPrintName() {
        assertEquals("Треугольник", triangle.getPrintName());
    }
}
