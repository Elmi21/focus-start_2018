package ru.cft.fs.minigalieva.task1;

public class ViewMultiplicationTable {

    private MultiplicationTable multiplicationTable;
    private int tableSize;
    private int cellSize;
    private int firstCellSize;

    ViewMultiplicationTable(MultiplicationTable multiplicationTable) {

        this.multiplicationTable = multiplicationTable;
        tableSize = multiplicationTable.getTableSize();
        firstCellSize = getDigitsNumber(tableSize);
        cellSize = getDigitsNumber(tableSize * tableSize);
    }

    private int getDigitsNumber(int number) {
        return String.valueOf(Math.abs(number)).length();
    }

    private String buildBorder() {
        StringBuilder border = new StringBuilder();
        border.append("");
        for (int i = 0; i < this.firstCellSize; i++)
            border.append("-");
        for (int i = 0; i < this.tableSize; i++) {
            border.append("+");
            for (int j = 0; j < this.cellSize; j++)
                border.append("-");
        }
        return border.toString();
    }

    public void display() {
        String firstCellFormat = "%" + this.firstCellSize + "s";
        String cellFormat = "|%" + this.cellSize + "s";
        String border = buildBorder();
        for (int i = 0; i < this.tableSize + 1; i++) {
            StringBuilder str = new StringBuilder();
            str.append(String.format(firstCellFormat, (i == 0) ? " " : (i + "")));
            for (int j = 0; j < this.tableSize; j++) {
                str.append(String.format(cellFormat, (i == 0) ? ((j + 1) + "") : (multiplicationTable.calcValue(i, j + 1) + "")));
            }
            System.out.println(str);
            System.out.println(border);
        }
    }

}
