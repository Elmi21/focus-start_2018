package ru.cft.fs.minigalieva.task1;

import java.util.Scanner;

class Task1 {
    private static final int MIN_TABLE_SIZE = 1;
    private static final int MAX_TABLE_SIZE = 32;

    public static void main(String[] consoleParameters) {
        System.out.println("Enter the size of the multiplication table:");
        try (Scanner in = new Scanner(System.in)) {
            String strTableSize;
            int tableSize;
            strTableSize = in.nextLine();
            tableSize = Integer.parseInt(strTableSize);
            if (tableSize < MIN_TABLE_SIZE || tableSize > MAX_TABLE_SIZE) {
                System.out.println("The size of the table must be from " + MIN_TABLE_SIZE
                        + " to " + MAX_TABLE_SIZE + "!\n");
                return;
            }
            MultiplicationTable multiplicationTable = new MultiplicationTable(tableSize);
            ViewMultiplicationTable viewMultiplicationTable = new ViewMultiplicationTable(multiplicationTable);
            viewMultiplicationTable.display();
        } catch (NumberFormatException e) {
            System.out.println("The data does not match the integer data type! \n");
        }
    }
}
