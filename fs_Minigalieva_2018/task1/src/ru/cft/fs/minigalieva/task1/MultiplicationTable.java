package ru.cft.fs.minigalieva.task1;

public class MultiplicationTable {
    private int tableSize;

    MultiplicationTable(int tableSize) {
        this.tableSize = tableSize;
    }

    public int calcValue(int x, int y) {
        return x * y;
    }

    public int getTableSize() {
        return tableSize;
    }
}
