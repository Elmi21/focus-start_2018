package ru.cft.fs.minigalieva.task42.production;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.fs.minigalieva.task42.utils.Messages;

import static java.lang.Thread.sleep;

class Producer implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);
    private Storage storage;
    private int producerId;
    private int producerTime;

    Producer(Storage storage, int producerId, int producerTime) {
        this.storage=storage;
        this.producerId=producerId;
        this.producerTime=producerTime;
    }

    @Override
    public void run(){
        while(!Thread.currentThread().isInterrupted()){
            try {
                sleep(producerTime);
                storage.put(producerId);
            } catch (InterruptedException e) {
                logger.error(Messages.get("THREAD_INTERRUPTED"), Messages.get("PRODUCER"), producerId);
                Thread.currentThread().interrupt();
            }
        }
    }
}