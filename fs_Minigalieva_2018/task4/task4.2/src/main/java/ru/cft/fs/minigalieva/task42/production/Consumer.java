package ru.cft.fs.minigalieva.task42.production;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.fs.minigalieva.task42.utils.Messages;

import static java.lang.Thread.sleep;

class Consumer implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);
    private Storage storage;
    private int consumerId;
    private int consumerTime;

    Consumer(Storage storage, int consumerId, int consumerTime) {
        this.storage=storage;
        this.consumerId=consumerId;
        this.consumerTime=consumerTime;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                sleep(consumerTime);
                storage.get(consumerId);
            } catch (InterruptedException e) {
                logger.error(Messages.get("THREAD_INTERRUPTED"), Messages.get("CONSUMER"), consumerId);
                Thread.currentThread().interrupt();
            }
        }
    }
}
