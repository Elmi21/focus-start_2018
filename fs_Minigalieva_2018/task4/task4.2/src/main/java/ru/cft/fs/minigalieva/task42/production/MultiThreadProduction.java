package ru.cft.fs.minigalieva.task42.production;

import java.util.ArrayList;
import java.util.List;

public class MultiThreadProduction {

    private int maxStorageSize;
    private int producersCount;
    private int producerTime;
    private int consumersCount;
    private int consumerTime;
    private List<Thread> consumers;
    private List<Thread> producers;

    public MultiThreadProduction(int maxStorageSize, int producersCount, int producerTime, int consumersCount, int
            consumerTime) {
        this.maxStorageSize = maxStorageSize;
        this.producersCount = producersCount;
        this.producerTime = producerTime * 1000;
        this.consumersCount = consumersCount;
        this.consumerTime = consumerTime * 1000;
        consumers = new ArrayList<>();
        producers = new ArrayList<>();

    }

    public void stop() {
        for (Thread consumer : consumers) {
            consumer.interrupt();
        }
        for (Thread producer : producers) {
            producer.interrupt();
        }
    }

    public void process() {
        Thread localThread;
        Storage storage =new Storage(maxStorageSize);
         for (int i = 0; i < consumersCount; i++) {
            Consumer consumer = new Consumer(storage,i + 1, consumerTime);
            localThread=new Thread(consumer);
            consumers.add(localThread);
            localThread.start();
        }
        for (int i = 0; i < producersCount; i++) {
            Producer producer = new Producer(storage, i + 1, producerTime);
            localThread=new Thread(producer);
            producers.add(localThread);
            localThread.start();
        }
    }
}