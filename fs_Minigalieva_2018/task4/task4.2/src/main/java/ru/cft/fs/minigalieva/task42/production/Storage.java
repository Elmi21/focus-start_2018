package ru.cft.fs.minigalieva.task42.production;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.fs.minigalieva.task42.utils.Messages;

import java.util.ArrayList;
import java.util.List;

class Storage {
    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);
    private List<Resource> resources;
    private int maxStorageSize;
    private int resourceCounter;

    Storage(int maxStorageSize){
        this.maxStorageSize=maxStorageSize;
        resources=new ArrayList<>();
        resourceCounter = 1;
    }

    synchronized void get(int costumerId) throws InterruptedException {
        Resource currentResource;
        while (resources.isEmpty()) {
            wait();
        }
        currentResource = resources.remove(resources.size() - 1);
        logger.info(Messages.get("CONSUMER_TAKE_RESOURCE"), costumerId, currentResource.resourceId);
        logger.info(Messages.get("RESOURCES_NUMBER"), resources.size());
        notifyAll();
    }

    synchronized void put(int producerId) throws InterruptedException {
        Resource currentResource;
        while (resources.size() == maxStorageSize) {
            wait();
        }
        currentResource = new Resource(resourceCounter);
        resources.add(currentResource);
        resourceCounter++;
        logger.info(Messages.get("PRODUCER_PUT_RESOURCE"), producerId, currentResource.resourceId);
        logger.info(Messages.get("RESOURCES_NUMBER"), resources.size());
        notifyAll();
    }
}
