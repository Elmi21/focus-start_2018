package ru.cft.fs.minigalieva.task42.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.fs.minigalieva.task42.production.MultiThreadProduction;

import java.io.IOException;
import java.util.ResourceBundle;

public class Task42 {
    private static final ResourceBundle SETTINGS_BUNDLE = ResourceBundle.getBundle("Settings");
    private static final Logger logger = LoggerFactory.getLogger(Task42.class);
    public static void main(String[] args) {
        int storageSize = Integer.parseInt(SETTINGS_BUNDLE.getString("STORAGE_SIZE"));
        int manufacturers = Integer.parseInt(SETTINGS_BUNDLE.getString("MANUFACTURES"));
        int timeManufacture = Integer.parseInt(SETTINGS_BUNDLE.getString("TIME_MANUFACTURE"));
        int consumers = Integer.parseInt(SETTINGS_BUNDLE.getString("CONSUMERS"));
        int timeConsumer = Integer.parseInt(SETTINGS_BUNDLE.getString("TIME_CONSUMER"));
        MultiThreadProduction multiThreadProduction = new MultiThreadProduction(storageSize, manufacturers,
                timeManufacture, consumers, timeConsumer);
        multiThreadProduction.process();
        try {
            System.in.read();

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        multiThreadProduction.stop();
    }
}
