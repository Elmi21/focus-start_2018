package computations;

import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import utils.Messages;

public class Task implements Callable<BigDecimal> {
    private static final Logger logger = LoggerFactory.getLogger(Task.class);
    private int fromNumber;
    private int toNumber;
    private BigDecimal threadResult;
    private int threadNum;

    Task(int fromNumber, int toNumber, int threadNum) {
        this.threadNum=threadNum;
        this.fromNumber = fromNumber;
        this.toNumber = toNumber;
        threadResult = BigDecimal.ZERO;

    }
    @Override
    public BigDecimal call() {
        int numIterations=toNumber-fromNumber;
        BigDecimal base=new BigDecimal("0.25");
        for (int i = fromNumber; i < toNumber; i++) {
            threadResult=threadResult.add(base.pow(i));
        }
        logger.info(Messages.get("THREAD_RESULT"), new Date(), threadNum, numIterations, fromNumber, toNumber, threadResult);

        return threadResult;

    }
}
