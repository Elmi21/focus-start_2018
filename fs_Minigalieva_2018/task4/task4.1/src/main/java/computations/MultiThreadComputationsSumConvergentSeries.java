package computations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.Messages;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MultiThreadComputationsSumConvergentSeries {
    private static final Logger logger = LoggerFactory.getLogger(MultiThreadComputationsSumConvergentSeries.class);
    private int numbersCount;
    private int threadsCount;
    private List<Future<BigDecimal>> futures = new ArrayList<>();
    private BigDecimal result = BigDecimal.ZERO;

    public MultiThreadComputationsSumConvergentSeries(int numbersCount, int threadsCount) {
        this.numbersCount = numbersCount;
        this.threadsCount = threadsCount;
    }

    public void process() throws ExecutionException, InterruptedException {
        Task taskCallable;
        int tasksNum = numbersCount / threadsCount;
        int remainTasksNum = numbersCount % threadsCount;
        ExecutorService executor = Executors.newFixedThreadPool(threadsCount);
        for (int i = 0; i < threadsCount; i++) {
            if ((i == threadsCount - 1) && (remainTasksNum != 0)) {
                taskCallable = new Task(1 + i * tasksNum, (i + 1) * tasksNum +
                        remainTasksNum, i + 1);
            } else {
                taskCallable = new Task(1 + i * tasksNum, tasksNum * (i + 1),i + 1);
            }
            Future<BigDecimal> future = executor.submit(taskCallable);
            futures.add(future);
        }
        for (Future<BigDecimal> localFuture : futures) {
            try {
                result = result.add(localFuture.get());
            } catch (InterruptedException | ExecutionException e) {
                logger.error(Messages.get("THREAD_ERROR"), e);
                throw e;
            }
        }
        result = result.add(BigDecimal.ONE);
        logger.info(Messages.get("MULTI_THREAD_COMPUTATION_TASK_RESULT"), result);
        executor.shutdown();
    }
}
