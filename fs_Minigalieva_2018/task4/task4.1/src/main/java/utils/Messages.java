package utils;

import java.util.ResourceBundle;

public final class Messages {
    private static final ResourceBundle STRINGS_BUNDLE = ResourceBundle.getBundle("Messages", new UTF8Control());

    private Messages() {
    }

    public static String get(String key) {
        return STRINGS_BUNDLE.getString(key);
    }
}