package main;

import computations.MultiThreadComputationsSumConvergentSeries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.Messages;

import java.util.concurrent.ExecutionException;

public class Task41 {
    private static final Logger logger = LoggerFactory.getLogger(Task41.class);

    public static void main(String[] args)  {
        int numbersCount = 100000;
        int threadsCount = 9;

        MultiThreadComputationsSumConvergentSeries multiThreadComputationsSumConvergentSeries = new MultiThreadComputationsSumConvergentSeries
                (numbersCount, threadsCount);
        try {
            multiThreadComputationsSumConvergentSeries.process();
        } catch (ExecutionException | InterruptedException e) {
            logger.error(Messages.get("TASK_NOT_EXECUTE"), e);
        }
    }
}
